﻿// Decompiled with JetBrains decompiler
// Type: sysTema.ConnectionControl
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using FirebirdSql.Data.FirebirdClient;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace sysTema
{
  public class ConnectionControl : Form
  {
    private ProgramSettings settings;
    private DbAlias current;
    private IContainer components;
    private FbConnection fbConnection1;
    private Label hostLabel;
    private TextBox hostBox;
    private Label label1;
    private TextBox portBox;
    private ComboBox dialectCombo;
    private ComboBox charsetCombo;
    private Label label2;
    private Label label3;
    private Label pathLabel;
    private OpenFileDialog pickDBFile;
    private Button pickFileButton;
    private Label label4;
    private Label label;
    private Label label6;
    private TextBox loginBox;
    private TextBox passwordBox;
    private Button ok;
    private Button testButton;
    private Button cancelButton;
    private ListBox aliasesList;
    private Button saveAlias;
    private Button delAlias;
    private RadioButton setDefaultDB;
    private RadioButton showDialogAlways;
    private GroupBox groupBox1;
    private GroupBox groupBox2;
    private Label label5;
    private Label label7;
    private TextBox aliasname;
    private GroupBox groupBox3;
    private RadioButton saveToDb;
    private RadioButton saveTOFile;

    public DbAlias Connection
    {
      get
      {
        return this.current;
      }
    }

    public ConnectionControl(ProgramSettings settings)
      : this()
    {
      this.settings = settings;
    }

    public ConnectionControl()
    {
      this.InitializeComponent();
      this.Load += new EventHandler(this.ConnectionControl_Load);
    }

    private void ConnectionControl_Load(object sender, EventArgs e)
    {
      foreach (object obj in this.settings.Items)
        this.aliasesList.Items.Add(obj);
      if (this.settings.StartupAlias != null)
      {
        this.SetData(this.settings.StartupAlias);
        this.setDefaultDB.Checked = true;
      }
      else
        this.showDialogAlways.Checked = true;
      if (this.settings.Store == TargetStore.File)
        this.saveTOFile.Checked = true;
      else
        this.saveToDb.Checked = true;
    }

    private void pickFileButton_Click(object sender, EventArgs e)
    {
      this.SetDbFile();
    }

    private void SetDbFile()
    {
      if (DialogResult.OK != this.pickDBFile.ShowDialog())
        return;
      this.pathLabel.Text = this.pickDBFile.FileName;
    }

    private void saveAlias_Click(object sender, EventArgs e)
    {
      this.SaveAlias();
    }

    private void SaveAlias()
    {
      DbAlias data = this.GetData();
      this.settings.Items.Add(data);
      this.aliasesList.Items.Add((object) data);
    }

    private void delAlias_Click(object sender, EventArgs e)
    {
      this.DeleteAlias();
    }

    private void DeleteAlias()
    {
      this.settings.Items.Remove((DbAlias) this.aliasesList.SelectedItem);
      this.aliasesList.Items.Remove(this.aliasesList.SelectedItem);
    }

    private void testButton_Click(object sender, EventArgs e)
    {
      this.TestConnection();
    }

    private void TestConnection()
    {
      DbAlias data = this.GetData();
      FbConnection fbConnection = new FbConnection();
      try
      {
        fbConnection.ConnectionString = data.ConnectionString;
        fbConnection.Open();
        int num = (int) MessageBox.Show("Подключение успешно.");
      }
      catch (FbException ex)
      {
        int num = (int) MessageBox.Show("Ошибка подключения: " + ex.Message, "sysTema.Error.Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      finally
      {
        fbConnection.Close();
      }
    }

    private void SetData(DbAlias alias)
    {
      FbConnectionStringBuilder connectionStringBuilder = new FbConnectionStringBuilder();
      connectionStringBuilder.ConnectionString = alias.ConnectionString;
      this.charsetCombo.Text = connectionStringBuilder.Charset;
      this.hostBox.Text = connectionStringBuilder.DataSource;
      this.dialectCombo.Text = connectionStringBuilder.Dialect.ToString();
      this.passwordBox.Text = connectionStringBuilder.Password;
      this.loginBox.Text = connectionStringBuilder.UserID;
      this.pathLabel.Text = connectionStringBuilder.Database;
      this.hostBox.Text = connectionStringBuilder.DataSource;
      this.aliasname.Text = alias.AliasName;
    }

    private DbAlias GetData()
    {
      DbAlias dbAlias = new DbAlias();
      FbConnectionStringBuilder connectionStringBuilder = new FbConnectionStringBuilder();
      connectionStringBuilder.Charset = this.charsetCombo.Text;
      connectionStringBuilder.DataSource = this.hostBox.Text;
      connectionStringBuilder.Dialect = int.Parse(this.dialectCombo.Text);
      connectionStringBuilder.Password = this.passwordBox.Text;
      connectionStringBuilder.UserID = this.loginBox.Text;
      connectionStringBuilder.Database = this.pathLabel.Text;
      dbAlias.AliasName = this.aliasname.Text;
      dbAlias.ConnectionString = connectionStringBuilder.ToString();
      return dbAlias;
    }

    private void ok_Click(object sender, EventArgs e)
    {
      this.current = this.GetData();
      this.settings.StartupAlias = this.setDefaultDB.Checked ? this.current : (DbAlias) null;
      this.settings.Store = this.saveToDb.Checked ? TargetStore.Database : TargetStore.File;
    }

    private void aliasesList_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      this.SetData((DbAlias) this.aliasesList.SelectedItem);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
      {
        this.components.Dispose();
        this.current = (DbAlias) null;
        this.settings = (ProgramSettings) null;
      }
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ConnectionControl));
      this.fbConnection1 = new FbConnection();
      this.hostLabel = new Label();
      this.hostBox = new TextBox();
      this.label1 = new Label();
      this.portBox = new TextBox();
      this.dialectCombo = new ComboBox();
      this.charsetCombo = new ComboBox();
      this.label2 = new Label();
      this.label3 = new Label();
      this.pathLabel = new Label();
      this.pickDBFile = new OpenFileDialog();
      this.pickFileButton = new Button();
      this.label4 = new Label();
      this.label = new Label();
      this.label6 = new Label();
      this.loginBox = new TextBox();
      this.passwordBox = new TextBox();
      this.ok = new Button();
      this.testButton = new Button();
      this.cancelButton = new Button();
      this.aliasesList = new ListBox();
      this.saveAlias = new Button();
      this.delAlias = new Button();
      this.setDefaultDB = new RadioButton();
      this.showDialogAlways = new RadioButton();
      this.groupBox1 = new GroupBox();
      this.groupBox2 = new GroupBox();
      this.label7 = new Label();
      this.aliasname = new TextBox();
      this.label5 = new Label();
      this.groupBox3 = new GroupBox();
      this.saveToDb = new RadioButton();
      this.saveTOFile = new RadioButton();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      this.hostLabel.AutoSize = true;
      this.hostLabel.Location = new Point(11, 24);
      this.hostLabel.Name = "hostLabel";
      this.hostLabel.Size = new Size(31, 13);
      this.hostLabel.TabIndex = 0;
      this.hostLabel.Text = "Хост";
      this.hostBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.hostBox.Location = new Point(14, 40);
      this.hostBox.Name = "hostBox";
      this.hostBox.Size = new Size(87, 20);
      this.hostBox.TabIndex = 1;
      this.hostBox.Text = "localhost";
      this.label1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(115, 24);
      this.label1.Name = "label1";
      this.label1.Size = new Size(32, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Порт";
      this.portBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.portBox.Location = new Point(118, 40);
      this.portBox.Name = "portBox";
      this.portBox.Size = new Size(62, 20);
      this.portBox.TabIndex = 1;
      this.portBox.Text = "3050";
      this.dialectCombo.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.dialectCombo.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.dialectCombo.FormattingEnabled = true;
      this.dialectCombo.Items.AddRange(new object[2]
      {
        (object) "1",
        (object) "3"
      });
      this.dialectCombo.Location = new Point(200, 40);
      this.dialectCombo.Name = "dialectCombo";
      this.dialectCombo.Size = new Size(72, 21);
      this.dialectCombo.TabIndex = 2;
      this.dialectCombo.Text = "3";
      this.charsetCombo.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.charsetCombo.AutoCompleteMode = AutoCompleteMode.Append;
      this.charsetCombo.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.charsetCombo.FormattingEnabled = true;
      this.charsetCombo.Items.AddRange(new object[2]
      {
        (object) "win1251",
        (object) "unicode"
      });
      this.charsetCombo.Location = new Point(296, 40);
      this.charsetCombo.Name = "charsetCombo";
      this.charsetCombo.Size = new Size(83, 21);
      this.charsetCombo.TabIndex = 2;
      this.charsetCombo.Text = "win1251";
      this.label2.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(197, 24);
      this.label2.Name = "label2";
      this.label2.Size = new Size(75, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Диалект SQL";
      this.label3.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(293, 24);
      this.label3.Name = "label3";
      this.label3.Size = new Size(62, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Кодировка";
      this.pathLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.pathLabel.BackColor = SystemColors.Window;
      this.pathLabel.BorderStyle = BorderStyle.Fixed3D;
      this.pathLabel.Location = new Point(129, 71);
      this.pathLabel.Name = "pathLabel";
      this.pathLabel.Size = new Size(314, 20);
      this.pathLabel.TabIndex = 3;
      this.pathLabel.Text = "<Путь не задан>";
      this.pathLabel.TextAlign = ContentAlignment.MiddleLeft;
      this.pickDBFile.Filter = "Firebird|*.fdb|Interbase|*.gdb";
      this.pickFileButton.Location = new Point(89, 70);
      this.pickFileButton.Name = "pickFileButton";
      this.pickFileButton.Size = new Size(34, 23);
      this.pickFileButton.TabIndex = 4;
      this.pickFileButton.Text = "...";
      this.pickFileButton.UseVisualStyleBackColor = true;
      this.pickFileButton.Click += new EventHandler(this.pickFileButton_Click);
      this.label4.AutoSize = true;
      this.label4.Location = new Point(11, 75);
      this.label4.Name = "label4";
      this.label4.Size = new Size(72, 13);
      this.label4.TabIndex = 0;
      this.label4.Text = "База данных";
      this.label.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label.AutoSize = true;
      this.label.Location = new Point(397, 24);
      this.label.Name = "label";
      this.label.Size = new Size(38, 13);
      this.label.TabIndex = 5;
      this.label.Text = "Логин";
      this.label6.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label6.AutoSize = true;
      this.label6.Location = new Point(513, 24);
      this.label6.Name = "label6";
      this.label6.Size = new Size(45, 13);
      this.label6.TabIndex = 5;
      this.label6.Text = "Пароль";
      this.loginBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.loginBox.Location = new Point(400, 40);
      this.loginBox.Name = "loginBox";
      this.loginBox.Size = new Size(100, 20);
      this.loginBox.TabIndex = 6;
      this.loginBox.Text = "SYSDBA";
      this.passwordBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.passwordBox.Location = new Point(516, 40);
      this.passwordBox.Name = "passwordBox";
      this.passwordBox.PasswordChar = '*';
      this.passwordBox.Size = new Size(111, 20);
      this.passwordBox.TabIndex = 6;
      this.passwordBox.Text = "masterkey";
      this.ok.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.ok.DialogResult = DialogResult.OK;
      this.ok.Location = new Point(412, 326);
      this.ok.Name = "ok";
      this.ok.Size = new Size(106, 33);
      this.ok.TabIndex = 7;
      this.ok.Text = "ОК";
      this.ok.UseVisualStyleBackColor = true;
      this.ok.Click += new EventHandler(this.ok_Click);
      this.testButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.testButton.Location = new Point(448, 166);
      this.testButton.Name = "testButton";
      this.testButton.Size = new Size(179, 25);
      this.testButton.TabIndex = 7;
      this.testButton.Text = "Проверить подключение";
      this.testButton.UseVisualStyleBackColor = true;
      this.testButton.Click += new EventHandler(this.testButton_Click);
      this.cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.cancelButton.DialogResult = DialogResult.Cancel;
      this.cancelButton.Location = new Point(539, 326);
      this.cancelButton.Name = "cancelButton";
      this.cancelButton.Size = new Size(106, 33);
      this.cancelButton.TabIndex = 7;
      this.cancelButton.Text = "Отмена";
      this.cancelButton.UseVisualStyleBackColor = true;
      this.aliasesList.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.aliasesList.FormattingEnabled = true;
      this.aliasesList.IntegralHeight = false;
      this.aliasesList.Location = new Point(14, 122);
      this.aliasesList.Name = "aliasesList";
      this.aliasesList.Size = new Size(613, 38);
      this.aliasesList.TabIndex = 8;
      this.aliasesList.MouseDoubleClick += new MouseEventHandler(this.aliasesList_MouseDoubleClick);
      this.saveAlias.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.saveAlias.Location = new Point(14, 166);
      this.saveAlias.Name = "saveAlias";
      this.saveAlias.Size = new Size(143, 25);
      this.saveAlias.TabIndex = 7;
      this.saveAlias.Text = "Сохранить подключение";
      this.saveAlias.UseVisualStyleBackColor = true;
      this.saveAlias.Click += new EventHandler(this.saveAlias_Click);
      this.delAlias.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.delAlias.Location = new Point(169, 166);
      this.delAlias.Name = "delAlias";
      this.delAlias.Size = new Size(143, 25);
      this.delAlias.TabIndex = 7;
      this.delAlias.Text = "Удалить";
      this.delAlias.UseVisualStyleBackColor = true;
      this.delAlias.Click += new EventHandler(this.delAlias_Click);
      this.setDefaultDB.AutoSize = true;
      this.setDefaultDB.Location = new Point(26, 29);
      this.setDefaultDB.Name = "setDefaultDB";
      this.setDefaultDB.Size = new Size(216, 17);
      this.setDefaultDB.TabIndex = 9;
      this.setDefaultDB.TabStop = true;
      this.setDefaultDB.Text = "Загружать при старте выбранную БД";
      this.setDefaultDB.UseVisualStyleBackColor = true;
      this.showDialogAlways.AutoSize = true;
      this.showDialogAlways.Location = new Point(26, 52);
      this.showDialogAlways.Name = "showDialogAlways";
      this.showDialogAlways.Size = new Size(234, 17);
      this.showDialogAlways.TabIndex = 10;
      this.showDialogAlways.TabStop = true;
      this.showDialogAlways.Text = "Всегда показывать это диалоговое окно";
      this.showDialogAlways.UseVisualStyleBackColor = true;
      this.groupBox1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.groupBox1.Controls.Add((Control) this.setDefaultDB);
      this.groupBox1.Controls.Add((Control) this.showDialogAlways);
      this.groupBox1.Location = new Point(7, 211);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(312, 98);
      this.groupBox1.TabIndex = 11;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Старт программы";
      this.groupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox2.Controls.Add((Control) this.label7);
      this.groupBox2.Controls.Add((Control) this.label);
      this.groupBox2.Controls.Add((Control) this.hostLabel);
      this.groupBox2.Controls.Add((Control) this.aliasesList);
      this.groupBox2.Controls.Add((Control) this.testButton);
      this.groupBox2.Controls.Add((Control) this.label1);
      this.groupBox2.Controls.Add((Control) this.label2);
      this.groupBox2.Controls.Add((Control) this.delAlias);
      this.groupBox2.Controls.Add((Control) this.label3);
      this.groupBox2.Controls.Add((Control) this.saveAlias);
      this.groupBox2.Controls.Add((Control) this.hostBox);
      this.groupBox2.Controls.Add((Control) this.aliasname);
      this.groupBox2.Controls.Add((Control) this.portBox);
      this.groupBox2.Controls.Add((Control) this.dialectCombo);
      this.groupBox2.Controls.Add((Control) this.passwordBox);
      this.groupBox2.Controls.Add((Control) this.charsetCombo);
      this.groupBox2.Controls.Add((Control) this.label6);
      this.groupBox2.Controls.Add((Control) this.loginBox);
      this.groupBox2.Controls.Add((Control) this.pickFileButton);
      this.groupBox2.Controls.Add((Control) this.label5);
      this.groupBox2.Controls.Add((Control) this.label4);
      this.groupBox2.Controls.Add((Control) this.pathLabel);
      this.groupBox2.Location = new Point(7, 9);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(638, 196);
      this.groupBox2.TabIndex = 12;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Подключение";
      this.label7.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.label7.AutoSize = true;
      this.label7.Location = new Point(471, 75);
      this.label7.Name = "label7";
      this.label7.Size = new Size(38, 13);
      this.label7.TabIndex = 5;
      this.label7.Text = "Алиас";
      this.aliasname.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.aliasname.Location = new Point(516, 71);
      this.aliasname.Name = "aliasname";
      this.aliasname.Size = new Size(111, 20);
      this.aliasname.TabIndex = 1;
      this.label5.AutoSize = true;
      this.label5.Location = new Point(11, 106);
      this.label5.Name = "label5";
      this.label5.Size = new Size(114, 13);
      this.label5.TabIndex = 0;
      this.label5.Text = "Другие подключения";
      this.groupBox3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox3.Controls.Add((Control) this.saveToDb);
      this.groupBox3.Controls.Add((Control) this.saveTOFile);
      this.groupBox3.Location = new Point(330, 211);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new Size(315, 98);
      this.groupBox3.TabIndex = 13;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Сохранение";
      this.saveToDb.AutoSize = true;
      this.saveToDb.Location = new Point(31, 52);
      this.saveToDb.Name = "saveToDb";
      this.saveToDb.Size = new Size(102, 17);
      this.saveToDb.TabIndex = 0;
      this.saveToDb.TabStop = true;
      this.saveToDb.Text = "Сохранять в бд";
      this.saveToDb.UseVisualStyleBackColor = true;
      this.saveTOFile.AutoSize = true;
      this.saveTOFile.Location = new Point(31, 29);
      this.saveTOFile.Name = "saveTOFile";
      this.saveTOFile.Size = new Size(116, 17);
      this.saveTOFile.TabIndex = 0;
      this.saveTOFile.TabStop = true;
      this.saveTOFile.Text = "Сохранять в файл";
      this.saveTOFile.UseVisualStyleBackColor = true;
      this.AcceptButton = (IButtonControl) this.ok;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.CancelButton = (IButtonControl) this.cancelButton;
      this.ClientSize = new Size(657, 371);
      this.Controls.Add((Control) this.groupBox3);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.cancelButton);
      this.Controls.Add((Control) this.ok);
      this.DoubleBuffered = true;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.MinimumSize = new Size(665, 405);
      this.Name = "ConnectionControl";
      this.SizeGripStyle = SizeGripStyle.Show;
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Настройки пограммы";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
