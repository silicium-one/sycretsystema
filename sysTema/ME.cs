﻿// Decompiled with JetBrains decompiler
// Type: sysTema.XmlSerializerWrapper
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace sysTema
{
  public class XmlSerializerWrapper
  {
    private static Encoding encoding;

    [XmlIgnore]
    public static Encoding Encoding
    {
      get
      {
        return XmlSerializerWrapper.encoding;
      }
      set
      {
        XmlSerializerWrapper.encoding = value;
      }
    }

    protected static string Serialize(object instance)
    {
      Type type = instance.GetType();
      MemoryStream memoryStream = new MemoryStream();
      memoryStream.Flush();
      XmlTextWriter xmlTextWriter = new XmlTextWriter((Stream) memoryStream, XmlSerializerWrapper.Encoding);
      new XmlSerializer(type).Serialize((XmlWriter) xmlTextWriter, instance);
      xmlTextWriter.Close();
      byte[] array = memoryStream.ToArray();
      string str = new string(XmlSerializerWrapper.Encoding.GetChars(array, 1, array.Length - 1));
      memoryStream.Close();
      return str;
    }

    protected static object Deserialize(Type t, string source)
    {
      MemoryStream memoryStream = new MemoryStream(XmlSerializerWrapper.Encoding.GetBytes(source));
      XmlParserContext context = new XmlParserContext((XmlNameTable) null, (XmlNamespaceManager) null, (string) null, XmlSpace.Default, XmlSerializerWrapper.Encoding);
      XmlTextReader xmlTextReader = new XmlTextReader((Stream) memoryStream, XmlNodeType.Document, context);
      object obj = new XmlSerializer(t).Deserialize((XmlReader) xmlTextReader);
      xmlTextReader.Close();
      memoryStream.Close();
      return obj;
    }
  }
}
