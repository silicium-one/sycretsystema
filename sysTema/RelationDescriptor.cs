﻿// Decompiled with JetBrains decompiler
// Type: sysTema.RelationDescriptor
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

namespace sysTema
{
  public class RelationDescriptor
  {
    private string name;
    private string parentTable;
    private string childTable;
    private string linkField;
    private string linkType;
    private string caption;
    private int showInRightPart;

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public string ParentTable
    {
      get
      {
        return this.parentTable;
      }
      set
      {
        this.parentTable = value;
      }
    }

    public string ChildTable
    {
      get
      {
        return this.childTable;
      }
      set
      {
        this.childTable = value;
      }
    }

    public string LinkField
    {
      get
      {
        return this.linkField;
      }
      set
      {
        this.linkField = value;
      }
    }

    public string LinkType
    {
      get
      {
        return this.linkType;
      }
      set
      {
        this.linkType = value;
      }
    }

    public string Caption
    {
      get
      {
        return this.caption;
      }
      set
      {
        this.caption = value;
      }
    }

    public int ShowInRightPart
    {
      get
      {
        return this.showInRightPart;
      }
      set
      {
        this.showInRightPart = value;
      }
    }

    public RelationDescriptor()
    {
    }

    public RelationDescriptor(
      string name,
      string parent,
      string child,
      string field,
      int showInRightPart)
    {
      this.Name = name;
      this.Caption = name;
      this.ParentTable = parent;
      this.ChildTable = child;
      this.LinkField = field;
      this.LinkType = "OneToMany";
      this.ShowInRightPart = showInRightPart;
    }

    public RelationDescriptor(RelationDescriptorSerializer rds)
    {
      this.Name = rds.Name;
      this.Caption = rds.Caption;
      this.ParentTable = rds.ParentTable;
      this.ChildTable = rds.ChildTable;
      this.LinkField = rds.LinkField;
      this.LinkType = rds.LinkType;
      this.ShowInRightPart = rds.ShowInRightPart;
    }
  }
}
