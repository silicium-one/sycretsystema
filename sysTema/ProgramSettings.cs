﻿// Decompiled with JetBrains decompiler
// Type: sysTema.ProgramSettings
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;

namespace sysTema
{
  [XmlRoot("Settings")]
  [Serializable]
  public class ProgramSettings
  {
    public const string SettingsFileName = "RelationCustomizer.config";
    public TargetStore Store;
    public DbAlias StartupAlias;
    public int RoleID;
    [XmlArrayItem("Alias")]
    public List<DbAlias> Items;

    public ProgramSettings()
    {
      this.Items = new List<DbAlias>();
    }

    private static string SettingsFilePath
    {
      get
      {
        string str = Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", "");
        return str.Remove(str.LastIndexOf('/') + 1);
      }
    }

    public static string SettingsFileFullPath
    {
      get
      {
        return ProgramSettings.SettingsFilePath + "RelationCustomizer.config";
      }
    }

    public static ProgramSettings Load()
    {
      return XmlSerializerEx<ProgramSettings>.Deserialize(ProgramSettings.SettingsFileFullPath);
    }

    public void Save()
    {
      XmlSerializerEx<ProgramSettings>.Serialize(ProgramSettings.SettingsFileFullPath, this);
    }
  }
}
