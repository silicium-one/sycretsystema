﻿// Decompiled with JetBrains decompiler
// Type: sysTema.ColumnDescriptor
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

namespace sysTema
{
  public class ColumnDescriptor
  {
    private string name;
    private string caption;
    private int hidden;
    private int _readonly;
    private int showInEditor;
    private int showInParentalRelations;
    private int forceFarLink;
    private int showInRightPart;
    private int partOfEditorTabTitle;
    private int ordinal;
    private int treatAsBool;
    private int treatAsPaswd;
    private int treatAsPicture;
    private int treatAsTextarea;
    private int treatAsPhone;
    private int treatAsFile;

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public string Caption
    {
      get
      {
        return this.caption;
      }
      set
      {
        this.caption = value;
      }
    }

    public int Hidden
    {
      get
      {
        return this.hidden;
      }
      set
      {
        this.hidden = value;
      }
    }

    public int Readonly
    {
      get
      {
        return this._readonly;
      }
      set
      {
        this._readonly = value;
      }
    }

    public int ShowInEditor
    {
      get
      {
        return this.showInEditor;
      }
      set
      {
        this.showInEditor = value;
      }
    }

    public int ShowInParentalRelations
    {
      get
      {
        return this.showInParentalRelations;
      }
      set
      {
        this.showInParentalRelations = value;
      }
    }

    public int ForceFarLink
    {
      get
      {
        return this.forceFarLink;
      }
      set
      {
        this.forceFarLink = value;
      }
    }

    public int ShowInRightPart
    {
      get
      {
        return this.showInRightPart;
      }
      set
      {
        this.showInRightPart = value;
      }
    }

    public int PartOfEditorTabTitle
    {
      get
      {
        return this.partOfEditorTabTitle;
      }
      set
      {
        this.partOfEditorTabTitle = value;
      }
    }

    public int Ordinal
    {
      get
      {
        return this.ordinal;
      }
      set
      {
        this.ordinal = value;
      }
    }

    public int TreatAsBool
    {
      get
      {
        return this.treatAsBool;
      }
      set
      {
        this.treatAsBool = value;
      }
    }

    public int TreatAsPaswd
    {
      get
      {
        return this.treatAsPaswd;
      }
      set
      {
        this.treatAsPaswd = value;
      }
    }

    public int TreatAsPicture
    {
      get
      {
        return this.treatAsPicture;
      }
      set
      {
        this.treatAsPicture = value;
      }
    }

    public int TreatAsTeaxtarea
    {
      get
      {
        return this.treatAsTextarea;
      }
      set
      {
        this.treatAsTextarea = value;
      }
    }

    public int TreatAsPhone
    {
      get
      {
        return this.treatAsPhone;
      }
      set
      {
        this.treatAsPhone = value;
      }
    }

    public int TreatAsFile
    {
      get
      {
        return this.treatAsFile;
      }
      set
      {
        this.treatAsFile = value;
      }
    }

    public ColumnDescriptor(string name)
    {
      this.Name = name;
      this.Hidden = 0;
      this.Caption = name;
    }

    public ColumnDescriptor()
    {
      this.Hidden = 0;
    }

    public ColumnDescriptor(ColumnDescriptorSerializer cds)
    {
      this.Caption = cds.Caption;
      this.Hidden = cds.Hidden;
      this.Name = cds.Name;
      this.Readonly = cds.Readonly;
      this.ShowInEditor = cds.ShowInEditor;
      this.ShowInParentalRelations = cds.ShowInParentalRelations;
      this.ForceFarLink = cds.ForceFarLink;
      this.ShowInRightPart = cds.ShowInRightPart;
      this.PartOfEditorTabTitle = cds.PartOfEditorTabTitle;
      this.Ordinal = cds.Ordinal;
      this.TreatAsBool = cds.TreatAsBool;
      this.TreatAsPaswd = cds.TreatAsPaswd;
      this.TreatAsFile = cds.TreatAsFile;
      this.TreatAsTeaxtarea = cds.TreatAsTextarea;
      this.TreatAsPhone = cds.TreatAsPhone;
    }
  }
}
