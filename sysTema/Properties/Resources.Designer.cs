﻿// Decompiled with JetBrains decompiler
// Type: sysTema.Properties.Resources
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace sysTema.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) sysTema.Properties.Resources.resourceMan, (object) null))
          sysTema.Properties.Resources.resourceMan = new ResourceManager("sysTema.Properties.Resources", typeof (sysTema.Properties.Resources).Assembly);
        return sysTema.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return sysTema.Properties.Resources.resourceCulture;
      }
      set
      {
        sysTema.Properties.Resources.resourceCulture = value;
      }
    }

    internal static string AtributesMetaDataSql
    {
      get
      {
        return sysTema.Properties.Resources.ResourceManager.GetString("AtributesMetaDataSql", sysTema.Properties.Resources.resourceCulture);
      }
    }

    internal static string TablesMetaDataSql
    {
      get
      {
        return sysTema.Properties.Resources.ResourceManager.GetString("TablesMetaDataSql", sysTema.Properties.Resources.resourceCulture);
      }
    }
  }
}
