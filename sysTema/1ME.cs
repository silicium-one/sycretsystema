﻿// Decompiled with JetBrains decompiler
// Type: sysTema.MetaExtractor
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Windows.Forms;

namespace sysTema
{
  public static class MetaExtractor
  {
    private static FbConnection cnn = (FbConnection) null;
    private static DataSet tablesCache = new DataSet("Dictionaries");
    private const string relationsQuery = "select\r\n                                            ri2.rdb$relation_name relation,\r\n                                            ri1.rdb$relation_name child,\r\n                                            rs.rdb$field_name link_field\r\n                                            from\r\n                                            rdb$relation_constraints rc left join rdb$indices ri1\r\n                                            on(rc.rdb$index_name = ri1.rdb$index_name)\r\n                                            left join rdb$indices ri2\r\n                                            on(ri1.rdb$foreign_key = ri2.rdb$index_name)\r\n                                            left join rdb$index_segments rs\r\n                                            on(rs.rdb$index_name = ri1.rdb$index_name)\r\n                                            where\r\n                                            rc.rdb$constraint_type = 'FOREIGN KEY'\r\n                                            and (ri2.rdb$relation_name = '{0}' or ri1.rdb$relation_name = '{0}')";
    private static HybridDictionary relations;
    private static bool cacheData;
    private static bool useMultipleConnections;

    static MetaExtractor()
    {
      MetaExtractor.tablesCache.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
      MetaExtractor.relations = new HybridDictionary();
    }

    public static FbConnection Connection
    {
      get
      {
        return MetaExtractor.cnn;
      }
      set
      {
        MetaExtractor.cnn = value;
      }
    }

    public static bool UseMultipleConnections
    {
      get
      {
        return MetaExtractor.useMultipleConnections;
      }
      set
      {
        MetaExtractor.useMultipleConnections = value;
      }
    }

    public static bool CacheData
    {
      get
      {
        return MetaExtractor.cacheData;
      }
      set
      {
        MetaExtractor.cacheData = value;
      }
    }

    public static IDbConnection ConnectionClone
    {
      get
      {
        return !MetaExtractor.useMultipleConnections ? (IDbConnection) MetaExtractor.cnn : (IDbConnection) new FbConnection(MetaExtractor.cnn.ConnectionString);
      }
    }

    private static bool CheckConnectionAvailability()
    {
      if (MetaExtractor.cnn == null)
        throw new ArgumentNullException("MetaExtractor: Connection not initialized.");
      if (MetaExtractor.cnn.State != ConnectionState.Open)
      {
        try
        {
          MetaExtractor.cnn.Open();
        }
        catch (FbException ex)
        {
          int num = (int) MessageBox.Show("Unable to reach the datasource: " + ex.Message, "sysTema.Error.Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          return false;
        }
      }
      return true;
    }

    public static DataTable GetTableSchema(string tname)
    {
      if (MetaExtractor.tablesCache.Tables.Contains(tname))
      {
        if (MetaExtractor.cacheData)
          return MetaExtractor.tablesCache.Tables[tname];
        MetaExtractor.tablesCache.Tables.Remove(tname);
      }
      MetaExtractor.CheckConnectionAvailability();
      DataTable[] dataTableArray = (DataTable[]) null;
      string selectCommandText = string.Format("SELECT FIRST 10 * FROM \"{0}\"", (object) tname);
      try
      {
        IDataAdapter dataAdapter = (IDataAdapter) new FbDataAdapter(selectCommandText, MetaExtractor.cnn);
        dataTableArray = dataAdapter.FillSchema(MetaExtractor.tablesCache, SchemaType.Source);
        dataAdapter.Fill(MetaExtractor.tablesCache);
        dataTableArray[0].TableName = tname;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Firebird exception running sql " + selectCommandText + ": " + ex.Message, "sysTema.Error.Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      return dataTableArray[0];
    }

    public static MetaExtractor.TableMetaDescription GetTableRelations(string tname)
    {
      if (MetaExtractor.relations.Contains((object) tname))
      {
        if (MetaExtractor.cacheData)
          return (MetaExtractor.TableMetaDescription) MetaExtractor.relations[(object) tname];
        MetaExtractor.relations.Remove((object) tname);
      }
      MetaExtractor.CheckConnectionAvailability();
      FbCommand fbCommand = new FbCommand(string.Format("select\r\n                                            ri2.rdb$relation_name relation,\r\n                                            ri1.rdb$relation_name child,\r\n                                            rs.rdb$field_name link_field\r\n                                            from\r\n                                            rdb$relation_constraints rc left join rdb$indices ri1\r\n                                            on(rc.rdb$index_name = ri1.rdb$index_name)\r\n                                            left join rdb$indices ri2\r\n                                            on(ri1.rdb$foreign_key = ri2.rdb$index_name)\r\n                                            left join rdb$index_segments rs\r\n                                            on(rs.rdb$index_name = ri1.rdb$index_name)\r\n                                            where\r\n                                            rc.rdb$constraint_type = 'FOREIGN KEY'\r\n                                            and (ri2.rdb$relation_name = '{0}' or ri1.rdb$relation_name = '{0}')", (object) tname), MetaExtractor.cnn);
      MetaExtractor.TableMetaDescription tableMetaDescription = new MetaExtractor.TableMetaDescription(tname);
      using (IDataReader dataReader = (IDataReader) fbCommand.ExecuteReader())
      {
        while (dataReader.Read())
        {
          string str1 = dataReader[0].ToString().TrimEnd(' ');
          string str2 = dataReader[1].ToString().TrimEnd(' ');
          string str3 = dataReader[2].ToString().TrimEnd(' ');
          MetaExtractor.DataRelation dataRelation = new MetaExtractor.DataRelation();
          dataRelation.Field = str3;
          if (str1 == tname)
          {
            dataRelation.TableName = str2;
            tableMetaDescription.Childs[(object) str2] = (object) dataRelation;
          }
          else
          {
            dataRelation.TableName = str1;
            tableMetaDescription.Parents[(object) str1] = (object) dataRelation;
          }
        }
      }
      if (MetaExtractor.cacheData)
        MetaExtractor.relations[(object) tname] = (object) tableMetaDescription;
      return tableMetaDescription;
    }

    public class DataRelation
    {
      public string TableName;
      public string Field;
    }

    public class TableMetaDescription
    {
      public string TableName;
      public HybridDictionary Childs;
      public HybridDictionary Parents;

      public TableMetaDescription(string tablename)
      {
        this.TableName = tablename;
        this.Childs = new HybridDictionary();
        this.Parents = new HybridDictionary();
      }
    }

    public class FieldDescriptor
    {
      public string FieldName;
      public string TableName;

      private FieldDescriptor()
      {
      }

      public FieldDescriptor(string table, string field)
      {
        this.FieldName = field;
        this.TableName = table;
      }

      public static string CreateKey(string tablename, string fieldname)
      {
        return string.Format("{0}.{1}", (object) tablename, (object) fieldname);
      }

      public static implicit operator string(MetaExtractor.FieldDescriptor field)
      {
        return MetaExtractor.FieldDescriptor.CreateKey(field.TableName, field.FieldName);
      }

      public static MetaExtractor.FieldDescriptor FromString(string fieldDescr)
      {
        string[] strArray = fieldDescr.Split('.');
        return strArray.Length != 2 ? (MetaExtractor.FieldDescriptor) null : new MetaExtractor.FieldDescriptor(strArray[0], strArray[1]);
      }
    }

    public class TableDescriptor
    {
      private string tname;
      private DataTable schema;
      private MetaExtractor.TableMetaDescription relations;

      public string TableName
      {
        get
        {
          return this.tname;
        }
        set
        {
          this.tname = value;
        }
      }

      public DataTable TableSchema
      {
        get
        {
          return this.schema;
        }
        set
        {
          this.schema = value;
        }
      }

      public MetaExtractor.TableMetaDescription DataRelations
      {
        get
        {
          return this.relations;
        }
        set
        {
          this.relations = value;
        }
      }

      public TableDescriptor()
      {
      }

      public TableDescriptor(string table)
      {
        this.TableName = table;
        this.TableSchema = MetaExtractor.GetTableSchema(table);
        this.DataRelations = MetaExtractor.GetTableRelations(table);
      }
    }

    public class FieldList : Dictionary<string, MetaExtractor.FieldDescriptor>
    {
      public static explicit operator string(MetaExtractor.FieldList list)
      {
        string str = "";
        foreach (string key in list.Keys)
          str = str + key + ",";
        return str.TrimEnd(',');
      }

      public FieldList()
      {
      }

      public void Append(string tablename, string flist)
      {
        string str = flist;
        char[] chArray = new char[2]{ ',', '\n' };
        foreach (string field in str.Split(chArray))
        {
          if (field != string.Empty)
          {
            MetaExtractor.FieldDescriptor fieldDescriptor = new MetaExtractor.FieldDescriptor(tablename, field);
            this[(string) fieldDescriptor] = fieldDescriptor;
          }
        }
      }

      public FieldList(string tablename, string fields)
      {
        this.Append(tablename, fields);
      }
    }

    public class TableList : Dictionary<string, MetaExtractor.TableDescriptor>
    {
      public static explicit operator string(MetaExtractor.TableList list)
      {
        string str = "";
        foreach (string key in list.Keys)
          str = str + key + ",";
        return str.TrimEnd(',');
      }
    }

    public class QueryContext
    {
      private IDbCommand command;
      private MetaExtractor.TableList tables;
      private MetaExtractor.FieldList fields;
      private string whereClause;
      private int startRow;
      private int rowCount;

      public IDbCommand Command
      {
        get
        {
          return this.command;
        }
      }

      public MetaExtractor.TableList Tables
      {
        get
        {
          return this.tables;
        }
      }

      public MetaExtractor.FieldList Fields
      {
        get
        {
          return this.fields;
        }
      }

      public string WhereClause
      {
        get
        {
          return this.whereClause;
        }
        set
        {
          this.whereClause = value;
        }
      }

      public IDataParameterCollection Parameters
      {
        get
        {
          return this.command.Parameters;
        }
      }

      public int GetFromRow
      {
        get
        {
          return this.startRow;
        }
        set
        {
          this.startRow = value;
        }
      }

      public int RowCount
      {
        get
        {
          return this.rowCount;
        }
        set
        {
          this.rowCount = value;
        }
      }

      private QueryContext()
      {
        this.tables = new MetaExtractor.TableList();
        this.fields = new MetaExtractor.FieldList();
        this.command = (IDbCommand) new FbCommand();
      }

      public void LeftJoinTable(MetaExtractor.TableMetaDescription leftTable)
      {
        throw new NotImplementedException();
      }

      public void RightJoinTable(MetaExtractor.TableMetaDescription rightTable)
      {
        throw new NotImplementedException();
      }

      public void NaturalJoinTable(MetaExtractor.TableMetaDescription table)
      {
        throw new NotImplementedException();
      }

      public static MetaExtractor.QueryContext CreateEmptyInstance()
      {
        return new MetaExtractor.QueryContext();
      }

      public static MetaExtractor.QueryContext CreateInstance(
        string tablename,
        string field_list)
      {
        MetaExtractor.QueryContext queryContext = new MetaExtractor.QueryContext();
        queryContext.Tables[tablename] = new MetaExtractor.TableDescriptor(tablename);
        queryContext.Fields.Append(tablename, field_list);
        return queryContext;
      }

      public static MetaExtractor.QueryContext CreateInstance(
        string tablename,
        MetaExtractor.FieldList field_list)
      {
        MetaExtractor.QueryContext queryContext = new MetaExtractor.QueryContext();
        queryContext.Tables[tablename] = new MetaExtractor.TableDescriptor(tablename);
        queryContext.fields = field_list;
        return queryContext;
      }
    }
  }
}
