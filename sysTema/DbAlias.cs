﻿// Decompiled with JetBrains decompiler
// Type: sysTema.DbAlias
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;

namespace sysTema
{
  [Serializable]
  public class DbAlias
  {
    public string ConnectionString;
    public string AliasName;

    public override string ToString()
    {
      return string.Format("{0} [{1}]", (object) this.AliasName, (object) this.ConnectionString);
    }
  }
}
