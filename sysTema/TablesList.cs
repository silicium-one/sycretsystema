﻿// Decompiled with JetBrains decompiler
// Type: sysTema.TablesList
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System.Collections;
using System.Collections.Generic;

namespace sysTema
{
  public class TablesList : Dictionary<string, TableDescriptor>, IList<TableDescriptor>, ICollection<TableDescriptor>, IEnumerable<TableDescriptor>, IEnumerable
  {
    private ArrayList indexes = new ArrayList();

    public int IndexOf(TableDescriptor item)
    {
      return this.indexes.IndexOf((object) item.Name);
    }

    public void Insert(int index, TableDescriptor item)
    {
      this.indexes.Insert(index, (object) item.Name);
      this.Add(item.Name, item);
    }

    public void RemoveAt(int index)
    {
      this.Remove((string) this.indexes[index]);
      this.indexes.RemoveAt(index);
    }

    public TableDescriptor this[int index]
    {
      get
      {
        return this[(string) this.indexes[index]];
      }
      set
      {
        this.Insert(index, value);
      }
    }

    public void Add(TableDescriptor item)
    {
      this.indexes.Add((object) item.Name);
      this.Add(item.Name, item);
    }

    public bool Contains(TableDescriptor item)
    {
      return this.ContainsKey(item.Name);
    }

    public void CopyTo(TableDescriptor[] array, int arrayIndex)
    {
      if (arrayIndex >= this.indexes.Count)
        return;
      int index1 = 0;
      for (int index2 = arrayIndex; index2 < this.indexes.Count; ++index2)
      {
        array[index1] = this[(string) this.indexes[index2]];
        ++index1;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return this.indexes.IsReadOnly;
      }
    }

    public bool Remove(TableDescriptor item)
    {
      if (!this.ContainsValue(item))
        return false;
      this.Remove(item.Name);
      this.indexes.Remove((object) item.Name);
      return true;
    }

    public IEnumerator<TableDescriptor> GetEnumerator()
    {
      return (IEnumerator<TableDescriptor>) this.indexes.GetEnumerator();
    }
  }
}
