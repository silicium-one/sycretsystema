﻿// Decompiled with JetBrains decompiler
// Type: sysTema.ImportSettings
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

namespace sysTema
{
  public class ImportSettings
  {
    public bool MakeBackup;
    public string BackupTablesPrefix;
    public ImportMode Mode;
  }
}
