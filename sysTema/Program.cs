﻿// Decompiled with JetBrains decompiler
// Type: sysTema.Program
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.Windows.Forms;

namespace sysTema
{
  internal static class Program
  {
    [STAThread]
    private static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run((Form) new RelationsCustomizer(args));
    }
  }
}
