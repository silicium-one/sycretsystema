﻿// Decompiled with JetBrains decompiler
// Type: sysTema.DBDescriptorSerializer
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System.Collections;
using System.Xml.Serialization;

namespace sysTema
{
  [XmlRoot("DatabaseMetaData")]
  public class DBDescriptorSerializer
  {
    [XmlArrayItem("Table", typeof (TableDescriptorSerializer))]
    public ArrayList Tables;
    [XmlArrayItem("Relation", typeof (RelationDescriptorSerializer))]
    public ArrayList Relations;

    public DBDescriptorSerializer()
    {
      this.Tables = new ArrayList();
      this.Relations = new ArrayList();
    }

    public DBDescriptorSerializer(DBDescriptor db)
    {
      this.Tables = new ArrayList();
      this.Relations = new ArrayList();
      foreach (string key in db.Relations.Keys)
        this.Relations.Add((object) new RelationDescriptorSerializer(db.Relations[key]));
      foreach (string key in db.Tables.Keys)
        this.Tables.Add((object) new TableDescriptorSerializer(db.Tables[key]));
    }

    public DBDescriptor GetDbDescriptor()
    {
      DBDescriptor dbDescriptor = new DBDescriptor();
      foreach (TableDescriptorSerializer table in this.Tables)
        dbDescriptor.Tables.Add(table.Name, new TableDescriptor(table));
      foreach (RelationDescriptorSerializer relation in this.Relations)
        dbDescriptor.Relations.Add(relation.Name, new RelationDescriptor(relation));
      return dbDescriptor;
    }
  }
}
