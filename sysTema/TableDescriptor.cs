﻿// Decompiled with JetBrains decompiler
// Type: sysTema.TableDescriptor
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System.Collections.Generic;

namespace sysTema
{
  public class TableDescriptor
  {
    public ColumnList Columns;
    public string Caption;
    public string Name;

    public TableDescriptor()
    {
      this.Columns = new ColumnList();
    }

    public TableDescriptor(string name)
    {
      this.Name = name;
      this.Caption = name;
      this.Columns = new ColumnList();
    }

    public TableDescriptor(TableDescriptorSerializer tbs)
    {
      this.Name = tbs.Name;
      this.Caption = tbs.Caption;
      this.Columns = new ColumnList();
      foreach (ColumnDescriptorSerializer column in (List<ColumnDescriptorSerializer>) tbs.Columns)
        this.Columns.Add(column.Name, new ColumnDescriptor(column));
    }
  }
}
