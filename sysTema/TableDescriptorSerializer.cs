﻿// Decompiled with JetBrains decompiler
// Type: sysTema.TableDescriptorSerializer
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.Xml.Serialization;

namespace sysTema
{
  [Serializable]
  public class TableDescriptorSerializer
  {
    [XmlArrayItem(ElementName = "Column")]
    public ColumnDescriptorSerializerList Columns;
    [XmlAttribute]
    public string Caption;
    [XmlAttribute]
    public string Name;

    public TableDescriptorSerializer()
    {
      this.Columns = new ColumnDescriptorSerializerList();
    }

    public TableDescriptorSerializer(string name)
    {
      this.Name = name;
      this.Caption = name;
      this.Columns = new ColumnDescriptorSerializerList();
    }

    public TableDescriptorSerializer(TableDescriptor table)
    {
      this.Name = table.Name;
      this.Caption = table.Caption;
      this.Columns = new ColumnDescriptorSerializerList();
      foreach (string key in table.Columns.Keys)
        this.Columns.Add(new ColumnDescriptorSerializer(table.Columns[key]));
    }
  }
}
