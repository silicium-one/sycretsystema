﻿// Decompiled with JetBrains decompiler
// Type: sysTema.RelationsCustomizer
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace sysTema
{
    public class RelationsCustomizer : Form
    {
        private Dictionary<string, string> commandLineArgs = new Dictionary<string, string>();
        private DBDescriptor metaDB = new DBDescriptor();

        private string[] _tablesScript = new string[4]
                                             {
                                                 "CREATE TABLE META_TABLES (\r\n                                        ID         INTEGER NOT NULL,\r\n                                        TABLENAME  VARCHAR(256) NOT NULL,\r\n                                        CAPTION    VARCHAR(256),\r\n                                        ROLEID                INT\r\n                                        );"
                                                 ,
                                                 "CREATE GENERATOR GEN_META_TABLES_ID;",
                                                 "SET GENERATOR GEN_META_TABLES_ID TO 0;",
                                                 "ALTER TABLE META_TABLES ADD CONSTRAINT PK_META_TABLES PRIMARY KEY (ID);"
                                             };

        private string[] _atribScript = new string[4]
                                            {
                                                "CREATE TABLE META_ATTRIBUTES (\r\n                                    ID                    INTEGER NOT NULL,\r\n                                    TABLEID               INTEGER NOT NULL,\r\n                                    FIELD                 VARCHAR(256) NOT NULL,\r\n                                    CAPTION               VARCHAR(256),\r\n                                    HIDDEN                SMALLINT,\r\n                                    READONLY              SMALLINT,\r\n                                    EDITORAVAILABLE       SMALLINT,\r\n                                    PARENTTABLEAVAILABLE  SMALLINT,\r\n                                    FARLINK               SMALLINT,\r\n                                    SHOWINRIGHTPART       SMALLINT,\r\n                                    PARTOFEDITORTABTITLE  SMALLINT,\r\n                                    ORDINAL               SMALLINT,\r\n                                    TREATASBOOLEAN        SMALLINT,\r\n                                    TREATASPASSWORD       SMALLINT,\r\n                                    TREATASPICTURE       SMALLINT,\r\n                                    TREATASFILE          SMALLINT,\r\n                                    TREATASTEXTAREA       SMALLINT,\r\n\t\t\t\t\t\t\t\t\tTREATASPHONE         SMALLINT,\r\n                                    ROLEID                INT\r\n                                    );"
                                                ,
                                                "CREATE GENERATOR GEN_META_ATTRIBUTES_ID;",
                                                "SET GENERATOR GEN_META_ATTRIBUTES_ID TO 0;",
                                                "ALTER TABLE META_ATTRIBUTES ADD CONSTRAINT PK_META_ATTRIBUTES PRIMARY KEY (ID);"
                                            };

        private string[] _relationsScript = new string[4]
                                                {
                                                    "CREATE TABLE META_RELATIONS (\r\n                                    ID                    INTEGER NOT NULL,\r\n                                    TABLENAME             VARCHAR(256) NOT NULL,\r\n                                    CHILD                 VARCHAR(256) NOT NULL,\r\n                                    CAPTION               VARCHAR(256) NOT NULL,\r\n                                    SHOWINRIGHTPART       SMALLINT,\r\n                                    ROLEID                INT\r\n                                    );"
                                                    ,
                                                    "CREATE GENERATOR GEN_META_RELATIONS_ID;",
                                                    "SET GENERATOR GEN_META_RELATIONS_ID TO 0;",
                                                    "ALTER TABLE META_RELATIONS ADD CONSTRAINT PK_META_RELATIONS PRIMARY KEY (ID);"
                                                };

        private const string TablesTableName = "META_TABLES";
        private const string AtributesTableName = "META_ATTRIBUTES";
        private const string RelationsTableName = "META_RELATIONS";
        private const string CreateBackupSqlFormatString = "";

        private const string tablesSQL =
            "CREATE TABLE META_TABLES (\r\n                                        ID         INTEGER NOT NULL,\r\n                                        TABLENAME  VARCHAR(256) NOT NULL,\r\n                                        CAPTION    VARCHAR(256),\r\n                                        ROLEID                INT\r\n                                        );";

        private const string tablesSQLgen = "CREATE GENERATOR GEN_META_TABLES_ID;";
        private const string tablesSQLgenset = "SET GENERATOR GEN_META_TABLES_ID TO 0;";
        private const string tablesSQLpk = "ALTER TABLE META_TABLES ADD CONSTRAINT PK_META_TABLES PRIMARY KEY (ID);";

        private const string atrSQL =
            "CREATE TABLE META_ATTRIBUTES (\r\n                                    ID                    INTEGER NOT NULL,\r\n                                    TABLEID               INTEGER NOT NULL,\r\n                                    FIELD                 VARCHAR(256) NOT NULL,\r\n                                    CAPTION               VARCHAR(256),\r\n                                    HIDDEN                SMALLINT,\r\n                                    READONLY              SMALLINT,\r\n                                    EDITORAVAILABLE       SMALLINT,\r\n                                    PARENTTABLEAVAILABLE  SMALLINT,\r\n                                    FARLINK               SMALLINT,\r\n                                    SHOWINRIGHTPART       SMALLINT,\r\n                                    PARTOFEDITORTABTITLE  SMALLINT,\r\n                                    ORDINAL               SMALLINT,\r\n                                    TREATASBOOLEAN        SMALLINT,\r\n                                    TREATASPASSWORD       SMALLINT,\r\n                                    TREATASPICTURE       SMALLINT,\r\n                                    TREATASFILE          SMALLINT,\r\n                                    TREATASTEXTAREA       SMALLINT,\r\n\t\t\t\t\t\t\t\t\tTREATASPHONE         SMALLINT,\r\n                                    ROLEID                INT\r\n                                    );";

        private const string atrSQLgen = "CREATE GENERATOR GEN_META_ATTRIBUTES_ID;";
        private const string atrSQLgenset = "SET GENERATOR GEN_META_ATTRIBUTES_ID TO 0;";

        private const string atrSQLpk =
            "ALTER TABLE META_ATTRIBUTES ADD CONSTRAINT PK_META_ATTRIBUTES PRIMARY KEY (ID);";

        private const string atrSQLfk =
            "ALTER TABLE META_ATTRIBUTES ADD CONSTRAINT FK_META_ATTRIBUTES FOREIGN KEY (TABLEID) REFERENCES META_TABLES (ID);";

        private const string relSQL =
            "CREATE TABLE META_RELATIONS (\r\n                                    ID                    INTEGER NOT NULL,\r\n                                    TABLENAME             VARCHAR(256) NOT NULL,\r\n                                    CHILD                 VARCHAR(256) NOT NULL,\r\n                                    CAPTION               VARCHAR(256) NOT NULL,\r\n                                    SHOWINRIGHTPART       SMALLINT,\r\n                                    ROLEID                INT\r\n                                    );";

        private const string relSQLpk = "ALTER TABLE META_RELATIONS ADD CONSTRAINT PK_META_RELATIONS PRIMARY KEY (ID);";
        private const string relSQLgen = "CREATE GENERATOR GEN_META_RELATIONS_ID;";
        private const string relSQLgenset = "SET GENERATOR GEN_META_RELATIONS_ID TO 0;";
        private ProgramSettings settings;
        private IContainer components;
        private TreeView tablesTree;
        private ImageList treeImages;
        private ToolStrip toolStrip1;
        private FbConnection fbConnection;
        private TextBox tableCaption;
        private Label label3;
        private SaveFileDialog metaNameDlg;
        private SplitContainer splitContainer2;
        private SplitContainer splitContainer1;
        private DataGridView grid;
        private ToolStripButton settingsTBB;
        private BindingSource columnDescriptorSerializerBindingSource;
        private TabControl tabControl1;
        private TabPage tableColumnsPage;
        private TabPage linksPage;
        private FbCommand fbCommand4;
        private ToolStripDropDownButton toolStripDropDownButton1;
        private ToolStripMenuItem saveToFileMI;
        private ToolStripMenuItem importToDBMI;
        private FbCommand fbCommand5;
        private ToolStripButton openTBB;
        private OpenFileDialog openMetaDlg;
        private BindingSource bindingSource1;
        private DataGridView linksGrid;
        private BindingSource bindingSource2;
        private BindingSource relationDescriptorSerializerBindingSource;
        private DataGridViewTextBoxColumn parentTableDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn childTableDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn captionDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private DataGridViewTextBoxColumn showInEditorDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn captionDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn showInParentalRelationsDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn readonlyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn forceFarLinkDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn treatAsPaswdDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn showInRightPartDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn treatAsBoolDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn partOfEditorTabTitleDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn ordinalDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn hiddenDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn showInEditorDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn captionDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn showInParentalRelationsDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn readonlyDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn forceFarLinkDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn treatAsPaswdDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn showInRightPartDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn treatAsBoolDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn partOfEditorTabTitleDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn ordinalDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn hiddenDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn Ordinal;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private DataGridViewCheckBoxColumn ShowInRightPart;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private DataGridViewCheckBoxColumn ForceFarLink;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private DataGridViewCheckBoxColumn TreatAsPaswd;
        private DataGridViewCheckBoxColumn TreatAsBool;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private DataGridViewCheckBoxColumn TreatAsFile;
        private DataGridViewCheckBoxColumn TreatAsTextarea;
        private DataGridViewCheckBoxColumn TreatAsPhone;
        private DataGridViewCheckBoxColumn PartOfEditorTabTitle;

        public RelationsCustomizer(string[] args)
        {
            Regex regex = new Regex("^-?([^=]+)=([^=]+)$");
            foreach (string input in args)
            {
                if (regex.IsMatch(input))
                {
                    Match match = regex.Match(input);
                    this.commandLineArgs.Add(match.Groups[1].Value, match.Groups[2].Value);
                }
            }
            this.InitializeComponent();
        }

        public void SendMessage(string m)
        {
            if (this.commandLineArgs.ContainsKey("action"))
            {
                Console.WriteLine(m);
            }
            else
            {
                int num = (int) MessageBox.Show(m);
            }
        }

        private void RelationsCustomizer_Shown(object sender, EventArgs e)
        {
            this.StartWork();
        }

        private void RelationsCustomizer_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.FinishWork();
        }

        private void StartWork()
        {
            this.settings = ProgramSettings.Load();
            if (this.settings == null)
                this.settings = new ProgramSettings();
            if (this.settings.StartupAlias == null)
                return;
            if (this.commandLineArgs.ContainsKey("roleid"))
                this.settings.RoleID = int.Parse(this.commandLineArgs["roleid"]);
            if (string.IsNullOrEmpty(this.settings.StartupAlias.ConnectionString))
            {
                ConnectionControl connectionControl = new ConnectionControl(this.settings);
                if (connectionControl.ShowDialog() == DialogResult.OK)
                {
                    this.fbConnection.ConnectionString = connectionControl.Connection.ConnectionString;
                }
                else
                {
                    this.Close();
                    return;
                }
            }
            else
                this.fbConnection.ConnectionString = this.settings.StartupAlias.ConnectionString;
            try
            {
                this.fbConnection.Open();
                MetaExtractor.Connection = this.fbConnection;
                this.LoadMetaData();
                this.InitializeDictionariesNavigator();
            }
            catch (FbException ex)
            {
                this.SendMessage("Ошибка подключения к БД: " + ex.Message);
                this.Close();
                return;
            }
            if (this.commandLineArgs.ContainsKey("file"))
                this.LoadMetaDataFile(this.commandLineArgs["file"]);
            if (!this.commandLineArgs.ContainsKey("action") || !(this.commandLineArgs["action"] == "syncdb"))
                return;
            this.Save2DB();
            this.Close();
        }

        private void FinishWork()
        {
            this.fbConnection.Close();
            this.settings.Save();
        }

        private void LoadMetaData()
        {
            FbCommand fbCommand =
                new FbCommand(
                    "SELECT rdb$relation_name FROM rdb$relations WHERE rdb$view_source IS NULL AND rdb$flags IS NOT NULL order by rdb$relation_name",
                    this.fbConnection);
            ArrayList arrayList = new ArrayList();
            using (IDataReader dataReader = (IDataReader) fbCommand.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    string str = dataReader["rdb$relation_name"].ToString().TrimEnd(' ');
                    arrayList.Add((object) str);
                }
            }
            foreach (string index in arrayList)
            {
                MetaExtractor.QueryContext instance = MetaExtractor.QueryContext.CreateInstance(index, "*");
                MetaExtractor.TableMetaDescription dataRelations = instance.Tables[index].DataRelations;
                TableDescriptor tableDescriptor = new TableDescriptor(index);

                var tableQueryText = String.Format("SELECT * FROM META_TABLES WHERE ROLEID = {0} AND TABLENAME ='{1}'", settings.RoleID, index);
                var tableCommand = new FbCommand(tableQueryText, fbConnection);
                string tableID = "";
                using (var dataReader = (IDataReader)tableCommand.ExecuteReader())
                {
                    if (dataReader.Read())
                    {
                        tableDescriptor.Caption = dataReader["CAPTION"].ToString().Trim(' ');
                        tableID = dataReader["ID"].ToString().Trim(' ');
                    }
                }

                int num = 0;
                foreach (DataColumn column in (InternalDataCollectionBase)instance.Tables[index].TableSchema.Columns)
                {
                    ColumnDescriptor columnDescriptor = new ColumnDescriptor(column.ColumnName);
                    columnDescriptor.Ordinal = num++;
                    columnDescriptor.ShowInEditor = 1;
                    columnDescriptor.ShowInRightPart = 1;
                    if (column.DataType == typeof (sbyte))
                        columnDescriptor.TreatAsBool = 1;

                    var attrQueryText =
                        String.Format("SELECT * FROM META_ATTRIBUTES WHERE TABLEID = {0} AND FIELD = '{1}';", tableID,
                                      column.ColumnName);
                    var attrCommand = new FbCommand(attrQueryText, fbConnection);
                    using (var dataReader = (IDataReader) attrCommand.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            columnDescriptor.Caption = dataReader["CAPTION"].ToString().Trim(' ');
                            columnDescriptor.ForceFarLink = (short)dataReader["FARLINK"];
                            columnDescriptor.Hidden = (short) dataReader["HIDDEN"];
                            columnDescriptor.Ordinal = (short)dataReader["ORDINAL"];
                            columnDescriptor.PartOfEditorTabTitle = (short)dataReader["PARTOFEDITORTABTITLE"];
                            columnDescriptor.Readonly = (short)dataReader["READONLY"];
                            columnDescriptor.ShowInEditor = (short)dataReader["EDITORAVAILABLE"];
                            columnDescriptor.ShowInParentalRelations = (short)dataReader["PARENTTABLEAVAILABLE"];
                            columnDescriptor.ShowInRightPart = (short)dataReader["SHOWINRIGHTPART"];
                            columnDescriptor.TreatAsBool = (short)dataReader["TREATASBOOLEAN"];
                            columnDescriptor.TreatAsFile = (short)dataReader["TREATASFILE"];
                            columnDescriptor.TreatAsPaswd = (short)dataReader["TREATASPASSWORD"];
                            columnDescriptor.TreatAsPhone = (short)dataReader["TREATASPHONE"];
                            columnDescriptor.TreatAsPicture = (short)dataReader["TREATASPICTURE"];
                            columnDescriptor.TreatAsTeaxtarea = (short)dataReader["TREATASTEXTAREA"];
                        }
                    }

                    tableDescriptor.Columns[column.ColumnName] = columnDescriptor;
                }
                this.metaDB.Tables[index] = tableDescriptor;
                foreach (string key in (IEnumerable) dataRelations.Childs.Keys)
                {
                    MetaExtractor.DataRelation child = (MetaExtractor.DataRelation) dataRelations.Childs[(object) key];
                    this.metaDB.Relations[index.ToUpper() + key.ToUpper()] =
                        new RelationDescriptor(index.ToUpper() + key.ToUpper(), index, key, child.Field, 1);
                }
            }
        }

        private void InitializeDictionariesNavigator()
        {
            SortedList<string, TableDescriptor> sortedList = new SortedList<string, TableDescriptor>();
            foreach (TableDescriptor tableDescriptor in this.metaDB.Tables.Values)
                sortedList.Add(tableDescriptor.Name, tableDescriptor);
            foreach (TableDescriptor tableDescriptor in (IEnumerable<TableDescriptor>) sortedList.Values)
                this.tablesTree.Nodes.Add(new TreeNode(tableDescriptor.Name)
                                              {
                                                  Name = tableDescriptor.Name,
                                                  Tag = (object) tableDescriptor
                                              });
            List<RelationDescriptorSerializer> descriptorSerializerList = new List<RelationDescriptorSerializer>();
            foreach (RelationDescriptor rd in this.metaDB.Relations.Values)
                descriptorSerializerList.Add(new RelationDescriptorSerializer(rd));
            this.linksGrid.DataSource = (object) descriptorSerializerList;
            this.tablesTree.Focus();
        }

        private void UpdateDicitionariesNavigator()
        {
            List<string> stringList1 = new List<string>();
            foreach (TableDescriptor tableDescriptor in this.metaDB.Tables.Values)
            {
                TreeNode[] treeNodeArray = this.tablesTree.Nodes.Find(tableDescriptor.Name, true);
                if (treeNodeArray.Length > 0)
                {
                    Dictionary<string, ColumnDescriptor> dictionary =
                        (Dictionary<string, ColumnDescriptor>) new ColumnList();
                    foreach (
                        KeyValuePair<string, ColumnDescriptor> column in
                            (Dictionary<string, ColumnDescriptor>) tableDescriptor.Columns)
                        dictionary.Add(column.Key, column.Value);
                    foreach (KeyValuePair<string, ColumnDescriptor> keyValuePair in dictionary)
                    {
                        bool flag = true;
                        foreach (
                            KeyValuePair<string, ColumnDescriptor> column in
                                (Dictionary<string, ColumnDescriptor>) ((TableDescriptor) treeNodeArray[0].Tag).Columns)
                        {
                            if (column.Key == keyValuePair.Key)
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                            tableDescriptor.Columns.Remove(keyValuePair.Key);
                    }
                    foreach (
                        KeyValuePair<string, ColumnDescriptor> column in
                            (Dictionary<string, ColumnDescriptor>) ((TableDescriptor) treeNodeArray[0].Tag).Columns)
                    {
                        bool flag = true;
                        foreach (KeyValuePair<string, ColumnDescriptor> keyValuePair in dictionary)
                        {
                            if (keyValuePair.Key == column.Key)
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                            tableDescriptor.Columns.Add(column.Key, column.Value);
                    }
                }
                else
                    stringList1.Add(tableDescriptor.Name);
            }
            foreach (string key in stringList1)
                this.metaDB.Tables.Remove(key);
            foreach (TreeNode node in this.tablesTree.Nodes)
            {
                if (!this.metaDB.Tables.ContainsKey(node.Name))
                    this.metaDB.Tables.Add(node.Name, (TableDescriptor) node.Tag);
            }
            List<string> stringList2 = new List<string>();
            List<RelationDescriptorSerializer> descriptorSerializerList = new List<RelationDescriptorSerializer>();
            foreach (
                KeyValuePair<string, RelationDescriptor> relation in
                    (Dictionary<string, RelationDescriptor>) this.metaDB.Relations)
            {
                bool flag = true;
                foreach (
                    RelationDescriptorSerializer descriptorSerializer in
                        (List<RelationDescriptorSerializer>) this.linksGrid.DataSource)
                {
                    if (descriptorSerializer.Compare(relation.Value))
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    stringList2.Add(relation.Key);
            }
            foreach (
                RelationDescriptorSerializer descriptorSerializer in
                    (List<RelationDescriptorSerializer>) this.linksGrid.DataSource)
            {
                bool flag = true;
                foreach (
                    KeyValuePair<string, RelationDescriptor> relation in
                        (Dictionary<string, RelationDescriptor>) this.metaDB.Relations)
                {
                    if (descriptorSerializer.Compare(relation.Value))
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    descriptorSerializerList.Add(descriptorSerializer);
            }
            foreach (string key in stringList2)
                this.metaDB.Relations.Remove(key);
            foreach (RelationDescriptorSerializer rds in descriptorSerializerList)
                this.metaDB.Relations.Add(rds.Name, new RelationDescriptor(rds));
        }

        private void tablesTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.CustomizeRelations();
        }

        private void tablesTree_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            this.SaveCustomization();
        }

        private void CustomizeRelations()
        {
            if (!(this.tablesTree.SelectedNode.Tag is TableDescriptor))
                return;
            TableDescriptor tag = (TableDescriptor) this.tablesTree.SelectedNode.Tag;
            this.tableCaption.Text = tag.Caption == string.Empty ? tag.Name : tag.Caption;
            this.grid.DataSource = (object) new TableDescriptorSerializer(tag).Columns;
        }

        private void SaveCustomization()
        {
            if (this.tablesTree.SelectedNode == null || !(this.tablesTree.SelectedNode.Tag is TableDescriptor))
                return;
            TableDescriptor tag = (TableDescriptor) this.tablesTree.SelectedNode.Tag;
            this.grid.EndEdit();
            this.linksGrid.EndEdit();
            tag.Caption = this.tableCaption.Text;
            ColumnDescriptorSerializerList dataSource = (ColumnDescriptorSerializerList) this.grid.DataSource;
            for (int index = 0; index < dataSource.Count; ++index)
            {
                ColumnDescriptorSerializer descriptorSerializer = dataSource[index];
                ColumnDescriptor column = tag.Columns[descriptorSerializer.Name];
                column.Hidden = descriptorSerializer.Hidden;
                column.Readonly = descriptorSerializer.Readonly;
                column.ShowInEditor = descriptorSerializer.ShowInEditor;
                column.ShowInParentalRelations = descriptorSerializer.ShowInParentalRelations;
                column.Caption = descriptorSerializer.Caption;
                column.ForceFarLink = descriptorSerializer.ForceFarLink;
                column.ShowInRightPart = descriptorSerializer.ShowInRightPart;
                column.TreatAsBool = descriptorSerializer.TreatAsBool;
                column.TreatAsFile = descriptorSerializer.TreatAsFile;
                column.TreatAsTeaxtarea = descriptorSerializer.TreatAsTextarea;
                column.TreatAsPaswd = descriptorSerializer.TreatAsPaswd;
                column.TreatAsPicture = descriptorSerializer.TreatAsPicture;
                column.TreatAsPhone = descriptorSerializer.TreatAsPhone;
                column.Ordinal = descriptorSerializer.Ordinal;
                column.PartOfEditorTabTitle = descriptorSerializer.PartOfEditorTabTitle;
            }
            foreach (
                RelationDescriptorSerializer descriptorSerializer in
                    (List<RelationDescriptorSerializer>) this.linksGrid.DataSource)
            {
                RelationDescriptor relation = this.metaDB.Relations[descriptorSerializer.Name];
                relation.Caption = descriptorSerializer.Caption;
                relation.ShowInRightPart = descriptorSerializer.ShowInRightPart;
            }
        }

        private void settingsTBB_Click(object sender, EventArgs e)
        {
            int num = (int) new ConnectionControl(this.settings).ShowDialog();
        }

        public void DropTable(string table)
        {
            this.ExecuteNonQuery(string.Format("DROP TABLE '{0}'", (object) table));
        }

        public int ExecuteNonQuery(string sql)
        {
            if (sql == "")
                throw new ArgumentException("Sql command cannot be empty.");
            FbTransaction transaction = this.fbConnection.BeginTransaction();
            int num = new FbCommand(sql, this.fbConnection, transaction).ExecuteNonQuery();
            transaction.Commit();
            return num;
        }

        public bool TableExists(string tablename)
        {
            if (tablename == "")
                throw new ArgumentException("Table name is missing.");
            return
                (int)
                new FbCommand(
                    string.Format("SELECT count(*) FROM rdb$relations r WHERE r.rdb$relation_name = '{0}'",
                                  (object) tablename), this.fbConnection).ExecuteScalar() > 0;
        }

        private bool CheckExist(string tname, FbConnection cnn, FbTransaction transaction)
        {
            FbCommand fbCommand = new FbCommand();
            fbCommand.Connection = cnn;
            fbCommand.Transaction = transaction;
            fbCommand.CommandText =
                string.Format("SELECT count(*) FROM rdb$relations r WHERE r.rdb$relation_name = '{0}'", (object) tname);
            return (int) fbCommand.ExecuteScalar() > 0;
        }

        private void CreateBackup(string prefix)
        {
            string str1 = prefix + "META_TABLES";
            string str2 = prefix + "META_ATTRIBUTES";
            if (this.TableExists(str1))
                this.DropTable(str1);
            if (!this.TableExists(str2))
                return;
            this.DropTable(str2);
        }

        private void ExecuteTransactedCommand(string sql, FbConnection cnn, FbTransaction tr)
        {
            FbCommand fbCommand = new FbCommand();
            fbCommand.Connection = cnn;
            fbCommand.Transaction = tr;
            fbCommand.CommandText = sql;
            fbCommand.ExecuteNonQuery();
        }

        private void ExecuteStatements(
            string[] statements,
            FbConnection cnn,
            FbTransaction transaction)
        {
            FbCommand fbCommand = new FbCommand();
            fbCommand.Connection = cnn;
            fbCommand.Transaction = transaction;
            foreach (string statement in statements)
            {
                fbCommand.CommandText = statement;
                fbCommand.ExecuteNonQuery();
            }
        }

        private void Save2DB()
        {
            this.Cursor = Cursors.WaitCursor;
            this.SaveCustomization();
            FbTransaction transaction = this.fbConnection.BeginTransaction();
            if (!this.CheckExist("META_TABLES", this.fbConnection, transaction))
                this.ExecuteStatements(this._tablesScript, this.fbConnection, transaction);
            if (!this.CheckExist("META_ATTRIBUTES", this.fbConnection, transaction))
                this.ExecuteStatements(this._atribScript, this.fbConnection, transaction);
            if (!this.CheckExist("META_RELATIONS", this.fbConnection, transaction))
                this.ExecuteStatements(this._relationsScript, this.fbConnection, transaction);
            transaction.Commit();
            FbTransaction tr = this.fbConnection.BeginTransaction();
            this.ExecuteTransactedCommand(
                string.Format("DELETE FROM \"{0}\" WHERE ROLEID={1}", (object) "META_TABLES",
                              (object) this.settings.RoleID), this.fbConnection, tr);
            this.ExecuteTransactedCommand(
                string.Format("DELETE FROM \"{0}\" WHERE ROLEID={1}", (object) "META_ATTRIBUTES",
                              (object) this.settings.RoleID), this.fbConnection, tr);
            this.ExecuteTransactedCommand(
                string.Format("DELETE FROM \"{0}\" WHERE ROLEID={1}", (object) "META_RELATIONS",
                              (object) this.settings.RoleID), this.fbConnection, tr);
            tr.Commit();
            FbTransaction fbTransaction = this.fbConnection.BeginTransaction();
            FbCommand fbCommand = new FbCommand("SELECT GEN_ID(GEN_META_TABLES_ID,0) FROM RDB$DATABASE",
                                                this.fbConnection, fbTransaction);
            foreach (TableDescriptor tableDescriptor in this.metaDB.Tables.Values)
            {
                long num = (long) fbCommand.ExecuteScalar() + 1L;
                this.ExecuteTransactedCommand(
                    string.Format("INSERT INTO \"META_TABLES\" VALUES(GEN_ID(GEN_META_TABLES_ID,1),'{0}','{1}','{2}')",
                                  (object) tableDescriptor.Name, (object) tableDescriptor.Caption,
                                  (object) this.settings.RoleID), this.fbConnection, fbTransaction);
                foreach (ColumnDescriptor columnDescriptor in tableDescriptor.Columns.Values)
                    this.ExecuteTransactedCommand(
                        string.Format(
                            "INSERT INTO \"META_ATTRIBUTES\" VALUES(GEN_ID(GEN_META_ATTRIBUTES_ID,1),{0},'{1}','{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}, {17})",
                            (object) num, (object) columnDescriptor.Name, (object) columnDescriptor.Caption,
                            (object) columnDescriptor.Hidden, (object) columnDescriptor.Readonly,
                            (object) columnDescriptor.ShowInEditor, (object) columnDescriptor.ShowInParentalRelations,
                            (object) columnDescriptor.ForceFarLink, (object) columnDescriptor.ShowInRightPart,
                            (object) columnDescriptor.PartOfEditorTabTitle, (object) columnDescriptor.Ordinal,
                            (object) columnDescriptor.TreatAsBool, (object) this.settings.RoleID,
                            (object) columnDescriptor.TreatAsPaswd, (object) columnDescriptor.TreatAsPicture,
                            (object) columnDescriptor.TreatAsFile, (object) columnDescriptor.TreatAsTeaxtarea,
                            (object) columnDescriptor.TreatAsPhone), this.fbConnection, fbTransaction);
            }
            foreach (RelationDescriptor relationDescriptor in this.metaDB.Relations.Values)
                this.ExecuteTransactedCommand(
                    string.Format(
                        "INSERT INTO \"META_RELATIONS\" VALUES(GEN_ID(GEN_META_RELATIONS_ID,1),'{0}','{1}','{2}',{3},{4})",
                        (object) relationDescriptor.ParentTable, (object) relationDescriptor.ChildTable,
                        (object) relationDescriptor.Caption, (object) relationDescriptor.ShowInRightPart,
                        (object) this.settings.RoleID), this.fbConnection, fbTransaction);
            fbTransaction.Commit();
            this.Cursor = Cursors.Arrow;
            this.SendMessage("Заливка в базу прошла успешно.");
        }

        private void importToDBMI_Click(object sender, EventArgs e)
        {
            this.Save2DB();
        }

        private void saveToFileMI_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.SaveCustomization();
            if (DialogResult.OK == this.metaNameDlg.ShowDialog())
                MetaSerializer.Serialize(this.metaDB, this.metaNameDlg.FileName);
            this.Cursor = Cursors.Arrow;
        }

        private void openTBB_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != this.openMetaDlg.ShowDialog())
                return;
            this.LoadMetaDataFile(this.openMetaDlg.FileName);
        }

        private void LoadMetaDataFile(string filename)
        {
            this.metaDB = XmlSerializerEx<DBDescriptorSerializer>.Deserialize(filename).GetDbDescriptor();
            this.UpdateDicitionariesNavigator();
            this.tablesTree.Nodes.Clear();
            this.linksGrid.DataSource = (object) null;
            this.linksGrid.Rows.Clear();
            this.grid.DataSource = (object) null;
            this.InitializeDictionariesNavigator();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer) new Container();
            ComponentResourceManager componentResourceManager =
                new ComponentResourceManager(typeof (RelationsCustomizer));
            this.tablesTree = new TreeView();
            this.treeImages = new ImageList(this.components);
            this.tableCaption = new TextBox();
            this.label3 = new Label();
            this.toolStrip1 = new ToolStrip();
            this.settingsTBB = new ToolStripButton();
            this.toolStripDropDownButton1 = new ToolStripDropDownButton();
            this.saveToFileMI = new ToolStripMenuItem();
            this.importToDBMI = new ToolStripMenuItem();
            this.openTBB = new ToolStripButton();
            this.fbConnection = new FbConnection();
            this.metaNameDlg = new SaveFileDialog();
            this.splitContainer2 = new SplitContainer();
            this.grid = new DataGridView();
            this.bindingSource1 = new BindingSource(this.components);
            this.splitContainer1 = new SplitContainer();
            this.tabControl1 = new TabControl();
            this.tableColumnsPage = new TabPage();
            this.linksPage = new TabPage();
            this.linksGrid = new DataGridView();
            this.dataGridViewCheckBoxColumn5 = new DataGridViewCheckBoxColumn();
            this.parentTableDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.childTableDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.captionDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.relationDescriptorSerializerBindingSource = new BindingSource(this.components);
            this.fbCommand4 = new FbCommand();
            this.fbCommand5 = new FbCommand();
            this.openMetaDlg = new OpenFileDialog();
            this.showInEditorDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.captionDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.showInParentalRelationsDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.readonlyDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.forceFarLinkDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.treatAsPaswdDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.showInRightPartDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.treatAsBoolDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.partOfEditorTabTitleDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.ordinalDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.hiddenDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
            this.showInEditorDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.captionDataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
            this.showInParentalRelationsDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.readonlyDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.forceFarLinkDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.treatAsPaswdDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.showInRightPartDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.treatAsBoolDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.partOfEditorTabTitleDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.ordinalDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.hiddenDataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.columnDescriptorSerializerBindingSource = new BindingSource(this.components);
            this.bindingSource2 = new BindingSource(this.components);
            this.dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            this.Ordinal = new DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new DataGridViewCheckBoxColumn();
            this.ShowInRightPart = new DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new DataGridViewCheckBoxColumn();
            this.ForceFarLink = new DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new DataGridViewCheckBoxColumn();
            this.TreatAsPaswd = new DataGridViewCheckBoxColumn();
            this.TreatAsBool = new DataGridViewCheckBoxColumn();
            this.TreatAsFile = new DataGridViewCheckBoxColumn();
            this.TreatAsTextarea = new DataGridViewCheckBoxColumn();
            this.TreatAsPhone = new DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new DataGridViewCheckBoxColumn();
            this.PartOfEditorTabTitle = new DataGridViewCheckBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((ISupportInitialize) this.grid).BeginInit();
            ((ISupportInitialize) this.bindingSource1).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tableColumnsPage.SuspendLayout();
            this.linksPage.SuspendLayout();
            ((ISupportInitialize) this.linksGrid).BeginInit();
            ((ISupportInitialize) this.relationDescriptorSerializerBindingSource).BeginInit();
            ((ISupportInitialize) this.columnDescriptorSerializerBindingSource).BeginInit();
            ((ISupportInitialize) this.bindingSource2).BeginInit();
            this.SuspendLayout();
            this.tablesTree.BorderStyle = BorderStyle.None;
            this.tablesTree.Dock = DockStyle.Fill;
            this.tablesTree.FullRowSelect = true;
            this.tablesTree.HideSelection = false;
            this.tablesTree.ImageIndex = 0;
            this.tablesTree.ImageList = this.treeImages;
            this.tablesTree.Location = new Point(0, 0);
            this.tablesTree.Name = "tablesTree";
            this.tablesTree.SelectedImageIndex = 0;
            this.tablesTree.Size = new Size(163, 464);
            this.tablesTree.TabIndex = 0;
            this.tablesTree.AfterSelect += new TreeViewEventHandler(this.tablesTree_AfterSelect);
            this.tablesTree.BeforeSelect += new TreeViewCancelEventHandler(this.tablesTree_BeforeSelect);
            this.treeImages.ImageStream =
                (ImageListStreamer) componentResourceManager.GetObject("treeImages.ImageStream");
            this.treeImages.TransparentColor = Color.Transparent;
            this.treeImages.Images.SetKeyName(0, "folder_c.ico");
            this.tableCaption.Location = new Point(96, 5);
            this.tableCaption.Name = "tableCaption";
            this.tableCaption.Size = new Size(405, 20);
            this.tableCaption.TabIndex = 1;
            this.label3.AutoSize = true;
            this.label3.Location = new Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new Size(87, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ярлык таблицы";
            this.toolStrip1.Items.AddRange(new ToolStripItem[3]
                                               {
                                                   (ToolStripItem) this.settingsTBB,
                                                   (ToolStripItem) this.toolStripDropDownButton1,
                                                   (ToolStripItem) this.openTBB
                                               });
            this.toolStrip1.Location = new Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new Size(857, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            this.settingsTBB.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.settingsTBB.Image = (Image) componentResourceManager.GetObject("settingsTBB.Image");
            this.settingsTBB.ImageTransparentColor = Color.Magenta;
            this.settingsTBB.Name = "settingsTBB";
            this.settingsTBB.Size = new Size(65, 22);
            this.settingsTBB.Text = "Настройки";
            this.settingsTBB.Click += new EventHandler(this.settingsTBB_Click);
            this.toolStripDropDownButton1.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new ToolStripItem[2]
                                                                     {
                                                                         (ToolStripItem) this.saveToFileMI,
                                                                         (ToolStripItem) this.importToDBMI
                                                                     });
            this.toolStripDropDownButton1.Image =
                (Image) componentResourceManager.GetObject("toolStripDropDownButton1.Image");
            this.toolStripDropDownButton1.ImageTransparentColor = Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new Size(75, 22);
            this.toolStripDropDownButton1.Text = "Сохранить";
            this.saveToFileMI.Name = "saveToFileMI";
            this.saveToFileMI.Size = new Size(156, 22);
            this.saveToFileMI.Text = "В файл";
            this.saveToFileMI.Click += new EventHandler(this.saveToFileMI_Click);
            this.importToDBMI.Name = "importToDBMI";
            this.importToDBMI.Size = new Size(156, 22);
            this.importToDBMI.Text = "Залить в базу";
            this.importToDBMI.Click += new EventHandler(this.importToDBMI_Click);
            this.openTBB.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.openTBB.Image = (Image) componentResourceManager.GetObject("openTBB.Image");
            this.openTBB.ImageTransparentColor = Color.Magenta;
            this.openTBB.Name = "openTBB";
            this.openTBB.Size = new Size(57, 22);
            this.openTBB.Text = "Открыть";
            this.openTBB.Click += new EventHandler(this.openTBB_Click);
            this.fbConnection.ConnectionString = componentResourceManager.GetString("fbConnection.ConnectionString");
            this.metaNameDlg.DefaultExt = "metadata";
            this.metaNameDlg.Filter = "Metadata files|*.metadata";
            this.splitContainer2.Dock = DockStyle.Fill;
            this.splitContainer2.Location = new Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = Orientation.Horizontal;
            this.splitContainer2.Panel1.Controls.Add((Control) this.label3);
            this.splitContainer2.Panel1.Controls.Add((Control) this.tableCaption);
            this.splitContainer2.Panel2.Controls.Add((Control) this.grid);
            this.splitContainer2.Size = new Size(682, 464);
            this.splitContainer2.SplitterDistance = 30;
            this.splitContainer2.TabIndex = 3;
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.AutoGenerateColumns = false;
            this.grid.BackgroundColor = SystemColors.ControlLight;
            this.grid.BorderStyle = BorderStyle.None;
            this.grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange((DataGridViewColumn) this.dataGridViewTextBoxColumn2,
                                       (DataGridViewColumn) this.dataGridViewTextBoxColumn1,
                                       (DataGridViewColumn) this.Ordinal,
                                       (DataGridViewColumn) this.dataGridViewCheckBoxColumn1,
                                       (DataGridViewColumn) this.ShowInRightPart,
                                       (DataGridViewColumn) this.dataGridViewCheckBoxColumn2,
                                       (DataGridViewColumn) this.ForceFarLink,
                                       (DataGridViewColumn) this.dataGridViewCheckBoxColumn3,
                                       (DataGridViewColumn) this.dataGridViewCheckBoxColumn4,
                                       (DataGridViewColumn) this.TreatAsPaswd, (DataGridViewColumn) this.TreatAsBool,
                                       (DataGridViewColumn) this.TreatAsFile, (DataGridViewColumn) this.TreatAsTextarea,
                                       (DataGridViewColumn) this.TreatAsPhone,
                                       (DataGridViewColumn) this.dataGridViewCheckBoxColumn6,
                                       (DataGridViewColumn) this.PartOfEditorTabTitle);
            this.grid.DataSource = (object) this.bindingSource1;
            this.grid.Dock = DockStyle.Fill;
            this.grid.Location = new Point(0, 0);
            this.grid.Name = "grid";
            this.grid.Size = new Size(682, 430);
            this.grid.TabIndex = 1;
            this.bindingSource1.DataSource = (object) typeof (ColumnDescriptorSerializer);
            this.splitContainer1.Dock = DockStyle.Fill;
            this.splitContainer1.Location = new Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Panel1.Controls.Add((Control) this.tablesTree);
            this.splitContainer1.Panel2.Controls.Add((Control) this.splitContainer2);
            this.splitContainer1.Size = new Size(849, 464);
            this.splitContainer1.SplitterDistance = 163;
            this.splitContainer1.TabIndex = 4;
            this.tabControl1.Controls.Add((Control) this.tableColumnsPage);
            this.tabControl1.Controls.Add((Control) this.linksPage);
            this.tabControl1.Dock = DockStyle.Fill;
            this.tabControl1.Location = new Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new Size(857, 490);
            this.tabControl1.TabIndex = 5;
            this.tableColumnsPage.Controls.Add((Control) this.splitContainer1);
            this.tableColumnsPage.Location = new Point(4, 22);
            this.tableColumnsPage.Name = "tableColumnsPage";
            this.tableColumnsPage.Size = new Size(849, 464);
            this.tableColumnsPage.TabIndex = 0;
            this.tableColumnsPage.Text = "Таблицы и колонки";
            this.linksPage.Controls.Add((Control) this.linksGrid);
            this.linksPage.Location = new Point(4, 22);
            this.linksPage.Name = "linksPage";
            this.linksPage.Size = new Size(849, 464);
            this.linksPage.TabIndex = 1;
            this.linksPage.Text = "Связи";
            this.linksGrid.AllowUserToAddRows = false;
            this.linksGrid.AllowUserToDeleteRows = false;
            this.linksGrid.AutoGenerateColumns = false;
            this.linksGrid.BackgroundColor = SystemColors.ControlLight;
            this.linksGrid.BorderStyle = BorderStyle.None;
            this.linksGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.linksGrid.Columns.AddRange((DataGridViewColumn) this.dataGridViewCheckBoxColumn5,
                                            (DataGridViewColumn) this.parentTableDataGridViewTextBoxColumn,
                                            (DataGridViewColumn) this.childTableDataGridViewTextBoxColumn,
                                            (DataGridViewColumn) this.captionDataGridViewTextBoxColumn);
            this.linksGrid.DataSource = (object) this.relationDescriptorSerializerBindingSource;
            this.linksGrid.Dock = DockStyle.Fill;
            this.linksGrid.Location = new Point(0, 0);
            this.linksGrid.Name = "linksGrid";
            this.linksGrid.Size = new Size(849, 464);
            this.linksGrid.TabIndex = 0;
            this.dataGridViewCheckBoxColumn5.DataPropertyName = "ShowInRightPart";
            this.dataGridViewCheckBoxColumn5.FalseValue = (object) "0";
            this.dataGridViewCheckBoxColumn5.Frozen = true;
            this.dataGridViewCheckBoxColumn5.HeaderText = "Отображать в правой части";
            this.dataGridViewCheckBoxColumn5.IndeterminateValue = (object) "0";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.TrueValue = (object) "1";
            this.parentTableDataGridViewTextBoxColumn.DataPropertyName = "ParentTable";
            this.parentTableDataGridViewTextBoxColumn.Frozen = true;
            this.parentTableDataGridViewTextBoxColumn.HeaderText = "Таблица";
            this.parentTableDataGridViewTextBoxColumn.Name = "parentTableDataGridViewTextBoxColumn";
            this.parentTableDataGridViewTextBoxColumn.ReadOnly = true;
            this.childTableDataGridViewTextBoxColumn.DataPropertyName = "ChildTable";
            this.childTableDataGridViewTextBoxColumn.Frozen = true;
            this.childTableDataGridViewTextBoxColumn.HeaderText = "Дочерняя таблица";
            this.childTableDataGridViewTextBoxColumn.Name = "childTableDataGridViewTextBoxColumn";
            this.childTableDataGridViewTextBoxColumn.ReadOnly = true;
            this.captionDataGridViewTextBoxColumn.DataPropertyName = "Caption";
            this.captionDataGridViewTextBoxColumn.Frozen = true;
            this.captionDataGridViewTextBoxColumn.HeaderText = "Ярлык связи";
            this.captionDataGridViewTextBoxColumn.Name = "captionDataGridViewTextBoxColumn";
            this.relationDescriptorSerializerBindingSource.DataSource = (object) typeof (RelationDescriptorSerializer);
            this.fbCommand4.CommandTimeout = 30;
            this.fbCommand5.CommandTimeout = 30;
            this.openMetaDlg.Filter = "Metadata files|*.metadata";
            this.openMetaDlg.Title = "Открыть файл";
            this.showInEditorDataGridViewTextBoxColumn.DataPropertyName = "ShowInEditor";
            this.showInEditorDataGridViewTextBoxColumn.HeaderText = "ShowInEditor";
            this.showInEditorDataGridViewTextBoxColumn.Name = "showInEditorDataGridViewTextBoxColumn";
            this.captionDataGridViewTextBoxColumn1.DataPropertyName = "Caption";
            this.captionDataGridViewTextBoxColumn1.HeaderText = "Caption";
            this.captionDataGridViewTextBoxColumn1.Name = "captionDataGridViewTextBoxColumn1";
            this.showInParentalRelationsDataGridViewTextBoxColumn.DataPropertyName = "ShowInParentalRelations";
            this.showInParentalRelationsDataGridViewTextBoxColumn.HeaderText = "ShowInParentalRelations";
            this.showInParentalRelationsDataGridViewTextBoxColumn.Name =
                "showInParentalRelationsDataGridViewTextBoxColumn";
            this.readonlyDataGridViewTextBoxColumn.DataPropertyName = "Readonly";
            this.readonlyDataGridViewTextBoxColumn.HeaderText = "Readonly";
            this.readonlyDataGridViewTextBoxColumn.Name = "readonlyDataGridViewTextBoxColumn";
            this.forceFarLinkDataGridViewTextBoxColumn.DataPropertyName = "ForceFarLink";
            this.forceFarLinkDataGridViewTextBoxColumn.HeaderText = "ForceFarLink";
            this.forceFarLinkDataGridViewTextBoxColumn.Name = "forceFarLinkDataGridViewTextBoxColumn";
            this.treatAsPaswdDataGridViewTextBoxColumn.DataPropertyName = "TreatAsPaswd";
            this.treatAsPaswdDataGridViewTextBoxColumn.HeaderText = "TreatAsPaswd";
            this.treatAsPaswdDataGridViewTextBoxColumn.Name = "treatAsPaswdDataGridViewTextBoxColumn";
            this.showInRightPartDataGridViewTextBoxColumn.DataPropertyName = "ShowInRightPart";
            this.showInRightPartDataGridViewTextBoxColumn.HeaderText = "ShowInRightPart";
            this.showInRightPartDataGridViewTextBoxColumn.Name = "showInRightPartDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.treatAsBoolDataGridViewTextBoxColumn.DataPropertyName = "TreatAsBool";
            this.treatAsBoolDataGridViewTextBoxColumn.HeaderText = "TreatAsBool";
            this.treatAsBoolDataGridViewTextBoxColumn.Name = "treatAsBoolDataGridViewTextBoxColumn";
            this.partOfEditorTabTitleDataGridViewTextBoxColumn.DataPropertyName = "PartOfEditorTabTitle";
            this.partOfEditorTabTitleDataGridViewTextBoxColumn.HeaderText = "PartOfEditorTabTitle";
            this.partOfEditorTabTitleDataGridViewTextBoxColumn.Name = "partOfEditorTabTitleDataGridViewTextBoxColumn";
            this.ordinalDataGridViewTextBoxColumn.DataPropertyName = "Ordinal";
            this.ordinalDataGridViewTextBoxColumn.HeaderText = "Ordinal";
            this.ordinalDataGridViewTextBoxColumn.Name = "ordinalDataGridViewTextBoxColumn";
            this.hiddenDataGridViewTextBoxColumn.DataPropertyName = "Hidden";
            this.hiddenDataGridViewTextBoxColumn.HeaderText = "Hidden";
            this.hiddenDataGridViewTextBoxColumn.Name = "hiddenDataGridViewTextBoxColumn";
            this.showInEditorDataGridViewTextBoxColumn1.DataPropertyName = "ShowInEditor";
            this.showInEditorDataGridViewTextBoxColumn1.HeaderText = "ShowInEditor";
            this.showInEditorDataGridViewTextBoxColumn1.Name = "showInEditorDataGridViewTextBoxColumn1";
            this.captionDataGridViewTextBoxColumn2.DataPropertyName = "Caption";
            this.captionDataGridViewTextBoxColumn2.HeaderText = "Caption";
            this.captionDataGridViewTextBoxColumn2.Name = "captionDataGridViewTextBoxColumn2";
            this.showInParentalRelationsDataGridViewTextBoxColumn1.DataPropertyName = "ShowInParentalRelations";
            this.showInParentalRelationsDataGridViewTextBoxColumn1.HeaderText = "ShowInParentalRelations";
            this.showInParentalRelationsDataGridViewTextBoxColumn1.Name =
                "showInParentalRelationsDataGridViewTextBoxColumn1";
            this.readonlyDataGridViewTextBoxColumn1.DataPropertyName = "Readonly";
            this.readonlyDataGridViewTextBoxColumn1.HeaderText = "Readonly";
            this.readonlyDataGridViewTextBoxColumn1.Name = "readonlyDataGridViewTextBoxColumn1";
            this.forceFarLinkDataGridViewTextBoxColumn1.DataPropertyName = "ForceFarLink";
            this.forceFarLinkDataGridViewTextBoxColumn1.HeaderText = "ForceFarLink";
            this.forceFarLinkDataGridViewTextBoxColumn1.Name = "forceFarLinkDataGridViewTextBoxColumn1";
            this.treatAsPaswdDataGridViewTextBoxColumn1.DataPropertyName = "TreatAsPaswd";
            this.treatAsPaswdDataGridViewTextBoxColumn1.HeaderText = "TreatAsPaswd";
            this.treatAsPaswdDataGridViewTextBoxColumn1.Name = "treatAsPaswdDataGridViewTextBoxColumn1";
            this.showInRightPartDataGridViewTextBoxColumn1.DataPropertyName = "ShowInRightPart";
            this.showInRightPartDataGridViewTextBoxColumn1.HeaderText = "ShowInRightPart";
            this.showInRightPartDataGridViewTextBoxColumn1.Name = "showInRightPartDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.treatAsBoolDataGridViewTextBoxColumn1.DataPropertyName = "TreatAsBool";
            this.treatAsBoolDataGridViewTextBoxColumn1.HeaderText = "TreatAsBool";
            this.treatAsBoolDataGridViewTextBoxColumn1.Name = "treatAsBoolDataGridViewTextBoxColumn1";
            this.partOfEditorTabTitleDataGridViewTextBoxColumn1.DataPropertyName = "PartOfEditorTabTitle";
            this.partOfEditorTabTitleDataGridViewTextBoxColumn1.HeaderText = "PartOfEditorTabTitle";
            this.partOfEditorTabTitleDataGridViewTextBoxColumn1.Name = "partOfEditorTabTitleDataGridViewTextBoxColumn1";
            this.ordinalDataGridViewTextBoxColumn1.DataPropertyName = "Ordinal";
            this.ordinalDataGridViewTextBoxColumn1.HeaderText = "Ordinal";
            this.ordinalDataGridViewTextBoxColumn1.Name = "ordinalDataGridViewTextBoxColumn1";
            this.hiddenDataGridViewTextBoxColumn1.DataPropertyName = "Hidden";
            this.hiddenDataGridViewTextBoxColumn1.HeaderText = "Hidden";
            this.hiddenDataGridViewTextBoxColumn1.Name = "hiddenDataGridViewTextBoxColumn1";
            this.columnDescriptorSerializerBindingSource.AllowNew = false;
            this.columnDescriptorSerializerBindingSource.DataSource = (object) typeof (ColumnDescriptorSerializer);
            this.bindingSource2.AllowNew = false;
            this.bindingSource2.DataSource = (object) typeof (RelationDescriptorSerializer);
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Атрибут";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Caption";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Наименование";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.Ordinal.DataPropertyName = "Ordinal";
            this.Ordinal.Frozen = true;
            this.Ordinal.HeaderText = "Порядковый номер";
            this.Ordinal.Name = "Ordinal";
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "ShowInEditor";
            this.dataGridViewCheckBoxColumn1.FalseValue = (object) "0";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Отображать в редакторе";
            this.dataGridViewCheckBoxColumn1.IndeterminateValue = (object) "1";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.TrueValue = (object) "1";
            this.ShowInRightPart.DataPropertyName = "ShowInRightPart";
            this.ShowInRightPart.FalseValue = (object) "0";
            this.ShowInRightPart.HeaderText = "Отображать в правой части";
            this.ShowInRightPart.IndeterminateValue = (object) "1";
            this.ShowInRightPart.Name = "ShowInRightPart";
            this.ShowInRightPart.TrueValue = (object) "1";
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "ShowInParentalRelations";
            this.dataGridViewCheckBoxColumn2.FalseValue = (object) "0";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Отображать в родительских таблицах";
            this.dataGridViewCheckBoxColumn2.IndeterminateValue = (object) "0";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.TrueValue = (object) "1";
            this.ForceFarLink.DataPropertyName = "ForceFarLink";
            this.ForceFarLink.FalseValue = (object) "0";
            this.ForceFarLink.HeaderText = "Загружать при глубоком связывании";
            this.ForceFarLink.IndeterminateValue = (object) "0";
            this.ForceFarLink.Name = "ForceFarLink";
            this.ForceFarLink.TrueValue = (object) "1";
            this.dataGridViewCheckBoxColumn3.DataPropertyName = "Readonly";
            this.dataGridViewCheckBoxColumn3.FalseValue = (object) "0";
            this.dataGridViewCheckBoxColumn3.HeaderText = "Только для чтения";
            this.dataGridViewCheckBoxColumn3.IndeterminateValue = (object) "0";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.TrueValue = (object) "1";
            this.dataGridViewCheckBoxColumn4.DataPropertyName = "Hidden";
            this.dataGridViewCheckBoxColumn4.FalseValue = (object) "0";
            this.dataGridViewCheckBoxColumn4.HeaderText = "Скрытый";
            this.dataGridViewCheckBoxColumn4.IndeterminateValue = (object) "0";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.TrueValue = (object) "1";
            this.TreatAsPaswd.DataPropertyName = "TreatAsPaswd";
            this.TreatAsPaswd.FalseValue = (object) "0";
            this.TreatAsPaswd.HeaderText = "Трактовать как пароль";
            this.TreatAsPaswd.IndeterminateValue = (object) "0";
            this.TreatAsPaswd.Name = "TreatAsPaswd";
            this.TreatAsPaswd.TrueValue = (object) "1";
            this.TreatAsBool.DataPropertyName = "TreatAsBool";
            this.TreatAsBool.FalseValue = (object) "0";
            this.TreatAsBool.HeaderText = "Трактовать как флаг";
            this.TreatAsBool.IndeterminateValue = (object) "0";
            this.TreatAsBool.Name = "TreatAsBool";
            this.TreatAsBool.Resizable = DataGridViewTriState.True;
            this.TreatAsBool.SortMode = DataGridViewColumnSortMode.Automatic;
            this.TreatAsBool.TrueValue = (object) "1";
            this.TreatAsFile.DataPropertyName = "TreatAsFile";
            this.TreatAsFile.FalseValue = (object) "0";
            this.TreatAsFile.HeaderText = "Трактовать как файл";
            this.TreatAsFile.IndeterminateValue = (object) "0";
            this.TreatAsFile.Name = "TreatAsFile";
            this.TreatAsFile.TrueValue = (object) "1";
            this.TreatAsTextarea.DataPropertyName = "TreatAsTextarea";
            this.TreatAsTextarea.FalseValue = (object) "0";
            this.TreatAsTextarea.HeaderText = "Трактовать как многострочный контрол";
            this.TreatAsTextarea.IndeterminateValue = (object) "0";
            this.TreatAsTextarea.Name = "TreatAsTextarea";
            this.TreatAsTextarea.TrueValue = (object) "1";
            this.TreatAsPhone.DataPropertyName = "TreatAsPhone";
            this.TreatAsPhone.FalseValue = (object) "0";
            this.TreatAsPhone.HeaderText = "Трактовать как телефон";
            this.TreatAsPhone.IndeterminateValue = (object) "0";
            this.TreatAsPhone.Name = "TreatAsPhone";
            this.TreatAsPhone.TrueValue = (object) "1";
            this.dataGridViewCheckBoxColumn6.DataPropertyName = "TreatAsPicture";
            this.dataGridViewCheckBoxColumn6.FalseValue = (object) "0";
            this.dataGridViewCheckBoxColumn6.HeaderText = "Трактовать как картинку";
            this.dataGridViewCheckBoxColumn6.IndeterminateValue = (object) "0";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.TrueValue = (object) "1";
            this.PartOfEditorTabTitle.DataPropertyName = "PartOfEditorTabTitle";
            this.PartOfEditorTabTitle.FalseValue = (object) "0";
            this.PartOfEditorTabTitle.HeaderText = "Показывать во вкладке редактора";
            this.PartOfEditorTabTitle.IndeterminateValue = (object) "0";
            this.PartOfEditorTabTitle.Name = "PartOfEditorTabTitle";
            this.PartOfEditorTabTitle.TrueValue = (object) "1";
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(857, 515);
            this.Controls.Add((Control) this.tabControl1);
            this.Controls.Add((Control) this.toolStrip1);
            this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
            this.Name = "RelationsCustomizer";
            this.Text = "RelationsCustomizer";
            this.Shown += new EventHandler(this.RelationsCustomizer_Shown);
            this.FormClosing += new FormClosingEventHandler(this.RelationsCustomizer_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((ISupportInitialize) this.grid).EndInit();
            ((ISupportInitialize) this.bindingSource1).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tableColumnsPage.ResumeLayout(false);
            this.linksPage.ResumeLayout(false);
            ((ISupportInitialize) this.linksGrid).EndInit();
            ((ISupportInitialize) this.relationDescriptorSerializerBindingSource).EndInit();
            ((ISupportInitialize) this.columnDescriptorSerializerBindingSource).EndInit();
            ((ISupportInitialize) this.bindingSource2).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}
