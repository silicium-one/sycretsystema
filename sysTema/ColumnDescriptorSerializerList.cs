﻿// Decompiled with JetBrains decompiler
// Type: sysTema.ColumnDescriptorSerializerList
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace sysTema
{
  public class ColumnDescriptorSerializerList : List<ColumnDescriptorSerializer>, IBindingList, IList, ICollection, IEnumerable
  {
    public void AddIndex(PropertyDescriptor property)
    {
      this.ListChanged((object) this, new ListChangedEventArgs(ListChangedType.ItemAdded, property));
    }

    public object AddNew()
    {
      return (object) null;
    }

    public bool AllowEdit
    {
      get
      {
        return true;
      }
    }

    public bool AllowNew
    {
      get
      {
        return false;
      }
    }

    public bool AllowRemove
    {
      get
      {
        return false;
      }
    }

    public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
    {
    }

    public int Find(PropertyDescriptor property, object key)
    {
      return -1;
    }

    public bool IsSorted
    {
      get
      {
        return false;
      }
    }

    public event ListChangedEventHandler ListChanged;

    public void RemoveIndex(PropertyDescriptor property)
    {
    }

    public void RemoveSort()
    {
    }

    public ListSortDirection SortDirection
    {
      get
      {
        return ListSortDirection.Ascending;
      }
    }

    public PropertyDescriptor SortProperty
    {
      get
      {
        return (PropertyDescriptor) null;
      }
    }

    public bool SupportsChangeNotification
    {
      get
      {
        return true;
      }
    }

    public bool SupportsSearching
    {
      get
      {
        return false;
      }
    }

    public bool SupportsSorting
    {
      get
      {
        return false;
      }
    }
  }
}
