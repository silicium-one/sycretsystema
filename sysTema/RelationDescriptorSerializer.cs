﻿// Decompiled with JetBrains decompiler
// Type: sysTema.RelationDescriptorSerializer
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.Xml.Serialization;

namespace sysTema
{
  [Serializable]
  public class RelationDescriptorSerializer
  {
    private string name;
    private string parentTable;
    private string childTable;
    private string linkField;
    private string linkType;
    private string caption;
    private int showInRightPart;

    [XmlAttribute]
    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public string ParentTable
    {
      get
      {
        return this.parentTable;
      }
      set
      {
        this.parentTable = value;
      }
    }

    public string ChildTable
    {
      get
      {
        return this.childTable;
      }
      set
      {
        this.childTable = value;
      }
    }

    public string LinkField
    {
      get
      {
        return this.linkField;
      }
      set
      {
        this.linkField = value;
      }
    }

    public string LinkType
    {
      get
      {
        return this.linkType;
      }
      set
      {
        this.linkType = value;
      }
    }

    [XmlAttribute]
    public string Caption
    {
      get
      {
        return this.caption;
      }
      set
      {
        this.caption = value;
      }
    }

    public int ShowInRightPart
    {
      get
      {
        return this.showInRightPart;
      }
      set
      {
        this.showInRightPart = value;
      }
    }

    public RelationDescriptorSerializer()
    {
    }

    public RelationDescriptorSerializer(
      string name,
      string parent,
      string child,
      string field,
      int showInRightPart)
    {
      this.Name = name;
      this.Caption = name;
      this.ParentTable = parent;
      this.ChildTable = child;
      this.LinkField = field;
      this.LinkType = "OneToMany";
      this.ShowInRightPart = showInRightPart;
    }

    public bool Compare(RelationDescriptor rd)
    {
      return this.ParentTable == rd.ParentTable && this.ChildTable == rd.ChildTable && this.LinkField == rd.LinkField;
    }

    public RelationDescriptorSerializer(RelationDescriptor rd)
    {
      this.Name = rd.Name;
      this.Caption = rd.Caption;
      this.ParentTable = rd.ParentTable;
      this.ChildTable = rd.ChildTable;
      this.LinkField = rd.LinkField;
      this.LinkType = rd.LinkType;
      this.ShowInRightPart = rd.ShowInRightPart;
    }
  }
}
