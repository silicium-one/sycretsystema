﻿// Decompiled with JetBrains decompiler
// Type: sysTema.MetaSerializer
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System.IO;
using System.Xml.Serialization;

namespace sysTema
{
  public static class MetaSerializer
  {
    public static void Serialize(DBDescriptor db, string fname)
    {
      using (TextWriter textWriter = (TextWriter) new StreamWriter(fname))
        new XmlSerializer(typeof (DBDescriptorSerializer)).Serialize(textWriter, (object) new DBDescriptorSerializer(db));
    }
  }
}
