﻿// Decompiled with JetBrains decompiler
// Type: sysTema.XmlSerializerEx`1
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.IO;
using System.Xml.Serialization;

namespace sysTema
{
  public class XmlSerializerEx<TType>
  {
    public static TType Deserialize(string file)
    {
      try
      {
        using (TextReader textReader = (TextReader) new StreamReader(file))
        {
          try
          {
            return (TType) new XmlSerializer(typeof (TType)).Deserialize(textReader);
          }
          catch (InvalidOperationException ex)
          {
            Console.WriteLine(ex.Message);
            return default (TType);
          }
        }
      }
      catch (FileNotFoundException ex)
      {
        Console.WriteLine(ex.Message);
        return default (TType);
      }
    }

    public static void Serialize(string file, TType instance)
    {
      using (TextWriter textWriter = (TextWriter) new StreamWriter(file))
      {
        try
        {
          new XmlSerializer(typeof (TType)).Serialize(textWriter, (object) instance);
        }
        catch
        {
        }
      }
    }
  }
}
