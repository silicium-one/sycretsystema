﻿// Decompiled with JetBrains decompiler
// Type: sysTema.DBImportSettingsDialog
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace sysTema
{
  public class DBImportSettingsDialog : Form
  {
    private ImportSettings settings;
    private IContainer components;
    private CheckBox backupFlag;
    private Button ok;
    private Button cancel;
    private RadioButton updateMode;
    private RadioButton overwriteMode;
    private GroupBox groupBox1;
    private GroupBox groupBox2;
    private TextBox tablesTableName;
    private TextBox prefixValue;
    private Label label4;
    private Label label1;
    private Label label3;
    private Label label2;
    private TextBox attrTableName;

    public ImportSettings Settings
    {
      get
      {
        return this.settings;
      }
    }

    public DBImportSettingsDialog()
    {
      this.InitializeComponent();
    }

    private void backupFlag_CheckedChanged(object sender, EventArgs e)
    {
      this.prefixValue.Enabled = this.backupFlag.Checked;
      if (!this.prefixValue.Enabled)
        return;
      this.prefixValue.SelectAll();
      this.prefixValue.Focus();
    }

    private void ok_Click(object sender, EventArgs e)
    {
      this.settings = new ImportSettings();
      this.settings.Mode = this.overwriteMode.Checked ? ImportMode.Overwrite : ImportMode.Update;
      this.settings.MakeBackup = this.backupFlag.Checked;
      this.settings.BackupTablesPrefix = this.prefixValue.Text;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.backupFlag = new CheckBox();
      this.ok = new Button();
      this.cancel = new Button();
      this.updateMode = new RadioButton();
      this.overwriteMode = new RadioButton();
      this.groupBox1 = new GroupBox();
      this.groupBox2 = new GroupBox();
      this.label3 = new Label();
      this.label2 = new Label();
      this.attrTableName = new TextBox();
      this.tablesTableName = new TextBox();
      this.prefixValue = new TextBox();
      this.label4 = new Label();
      this.label1 = new Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      this.backupFlag.AutoSize = true;
      this.backupFlag.Location = new Point(12, 87);
      this.backupFlag.Name = "backupFlag";
      this.backupFlag.Size = new Size(107, 17);
      this.backupFlag.TabIndex = 2;
      this.backupFlag.Text = "Создать backup";
      this.backupFlag.UseVisualStyleBackColor = true;
      this.backupFlag.CheckedChanged += new EventHandler(this.backupFlag_CheckedChanged);
      this.ok.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.ok.DialogResult = DialogResult.OK;
      this.ok.Location = new Point(51, 267);
      this.ok.Name = "ok";
      this.ok.Size = new Size(100, 34);
      this.ok.TabIndex = 4;
      this.ok.Text = "ОК";
      this.ok.UseVisualStyleBackColor = true;
      this.ok.Click += new EventHandler(this.ok_Click);
      this.cancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.cancel.DialogResult = DialogResult.Cancel;
      this.cancel.Location = new Point(197, 267);
      this.cancel.Name = "cancel";
      this.cancel.Size = new Size(100, 34);
      this.cancel.TabIndex = 5;
      this.cancel.Text = "Отмена";
      this.cancel.UseVisualStyleBackColor = true;
      this.updateMode.AutoSize = true;
      this.updateMode.Checked = true;
      this.updateMode.Location = new Point(39, 27);
      this.updateMode.Name = "updateMode";
      this.updateMode.Size = new Size(74, 17);
      this.updateMode.TabIndex = 0;
      this.updateMode.TabStop = true;
      this.updateMode.Text = "Обновить";
      this.updateMode.UseVisualStyleBackColor = true;
      this.overwriteMode.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.overwriteMode.AutoSize = true;
      this.overwriteMode.Location = new Point(152, 27);
      this.overwriteMode.Name = "overwriteMode";
      this.overwriteMode.Size = new Size(133, 17);
      this.overwriteMode.TabIndex = 1;
      this.overwriteMode.Text = "Переписать целиком";
      this.overwriteMode.UseVisualStyleBackColor = true;
      this.groupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox1.Controls.Add((Control) this.overwriteMode);
      this.groupBox1.Controls.Add((Control) this.updateMode);
      this.groupBox1.Location = new Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(325, 64);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Параметры сохранения";
      this.groupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox2.Controls.Add((Control) this.label3);
      this.groupBox2.Controls.Add((Control) this.label2);
      this.groupBox2.Controls.Add((Control) this.attrTableName);
      this.groupBox2.Controls.Add((Control) this.tablesTableName);
      this.groupBox2.Controls.Add((Control) this.prefixValue);
      this.groupBox2.Controls.Add((Control) this.label4);
      this.groupBox2.Controls.Add((Control) this.label1);
      this.groupBox2.Location = new Point(12, 110);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(325, 138);
      this.groupBox2.TabIndex = 5;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Backup";
      this.label3.AutoSize = true;
      this.label3.Location = new Point(43, 106);
      this.label3.Name = "label3";
      this.label3.Size = new Size(55, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "Атрибуты";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(46, 80);
      this.label2.Name = "label2";
      this.label2.Size = new Size(52, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Таблицы";
      this.attrTableName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.attrTableName.Enabled = false;
      this.attrTableName.Location = new Point(104, 103);
      this.attrTableName.Name = "attrTableName";
      this.attrTableName.Size = new Size(204, 20);
      this.attrTableName.TabIndex = 2;
      this.attrTableName.Text = "METABACKUP_META_ATRIBUTES";
      this.tablesTableName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.tablesTableName.Enabled = false;
      this.tablesTableName.Location = new Point(104, 77);
      this.tablesTableName.Name = "tablesTableName";
      this.tablesTableName.Size = new Size(204, 20);
      this.tablesTableName.TabIndex = 2;
      this.tablesTableName.Text = "METABACKUP_META_TABLES";
      this.prefixValue.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.prefixValue.Enabled = false;
      this.prefixValue.Location = new Point(184, 23);
      this.prefixValue.Name = "prefixValue";
      this.prefixValue.Size = new Size(124, 20);
      this.prefixValue.TabIndex = 3;
      this.prefixValue.Text = "METABACKUP_";
      this.label4.AutoSize = true;
      this.label4.Location = new Point(6, 55);
      this.label4.Name = "label4";
      this.label4.Size = new Size(221, 13);
      this.label4.TabIndex = 0;
      this.label4.Text = "Информация будет сохранена в таблицах:";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(6, 26);
      this.label1.Name = "label1";
      this.label1.Size = new Size(172, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Префикс таблиц backup-версии:";
      this.AcceptButton = (IButtonControl) this.ok;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.CancelButton = (IButtonControl) this.cancel;
      this.ClientSize = new Size(349, 313);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.cancel);
      this.Controls.Add((Control) this.ok);
      this.Controls.Add((Control) this.backupFlag);
      this.MaximizeBox = false;
      this.MaximumSize = new Size(500, 347);
      this.MinimizeBox = false;
      this.MinimumSize = new Size(357, 347);
      this.Name = "DBImportSettingsDialog";
      this.SizeGripStyle = SizeGripStyle.Show;
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Импорт в базу данных";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
