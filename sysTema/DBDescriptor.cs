﻿// Decompiled with JetBrains decompiler
// Type: sysTema.DBDescriptor
// Assembly: sysTema, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5E5C4A38-2348-41FC-9A48-AC2EBD6586C3
// Assembly location: C:\Users\mazay\Desktop\RelationsCustomizer\sysTema.exe

namespace sysTema
{
  public class DBDescriptor
  {
    public TablesList Tables;
    public RelationsList Relations;

    public DBDescriptor()
    {
      this.Tables = new TablesList();
      this.Relations = new RelationsList();
    }
  }
}
